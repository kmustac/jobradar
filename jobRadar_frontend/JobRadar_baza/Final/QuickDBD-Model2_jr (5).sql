#drop schema jobradardb;
#create schema jobradardb;

CREATE TABLE `Korisnik` (
    `idKorisnik` int  auto_increment ,
    `imeKorisnik` varchar(50)  NOT NULL ,
    `prezimeKorisnik` varchar(50)  NOT NULL ,
    `emailKorisnik` varchar(50)  NOT NULL ,
    `lozinkaKorisnik` varchar(50)  NOT NULL ,
    `putDoProfilne` varchar(50) ,
    `putDoCV` varchar(50) ,
    `jeAdmin` boolean  NOT NULL ,
    `idStupanj` int ,
    PRIMARY KEY (
        `idKorisnik`
    ),
    CONSTRAINT `uc_Korisnik_emailKorisnik` UNIQUE (
        `emailKorisnik`
    )
);

CREATE TABLE `Firma` (
    `idFirma` int  auto_increment ,
    `imeFirma` varchar(50)  NOT NULL ,
    `emailFirma` varchar(50)  NOT NULL ,
    `telefonFirma` varchar(50) ,
    `putDoLogoFirma` varchar(50) ,
    PRIMARY KEY (
        `idFirma`
    ),
    CONSTRAINT `uc_Firma_emailFirma` UNIQUE (
        `emailFirma`
    )
);

CREATE TABLE `KorisnikRadiUFirmi` (
    `idKorisnik` int  NOT NULL ,
    `idFirma` int  NOT NULL ,
    `datumRada` date  NOT NULL ,
    PRIMARY KEY (
        `idKorisnik`,`idFirma`,`datumRada`
    )
);

CREATE TABLE `StupanjObrazovanja` (
    `idStupanj` int  auto_increment ,
    `imeStupanj` varchar(50)  NOT NULL ,
    PRIMARY KEY (
        `idStupanj`
    ),
    CONSTRAINT `uc_StupanjObrazovanja_imeStupanj` UNIQUE (
        `imeStupanj`
    )
);

CREATE TABLE `Znanja` (
    `idZnanje` int  auto_increment ,
    `imeZnanje` varchar(50)  NOT NULL ,
    `idKategorija` int  NOT NULL ,
    PRIMARY KEY (
        `idZnanje`
    ),
    CONSTRAINT `uc_Znanja_imeZnanje` UNIQUE (
        `imeZnanje`
    )
);

CREATE TABLE `KorisnikZnanja` (
    `idKorisnik` int  NOT NULL ,
    `idZnanje` int  NOT NULL ,
    PRIMARY KEY (
        `idKorisnik`,`idZnanje`
    )
);

CREATE TABLE `Kategorija` (
    `idKategorija` int  auto_increment ,
    `imeKategorija` varchar(50)  NOT NULL ,
    PRIMARY KEY (
        `idKategorija`
    ),
    CONSTRAINT `uc_Kategorija_imeKategorija` UNIQUE (
        `imeKategorija`
    )
);

CREATE TABLE `Komentira` (
    `idKorisnik` int  NOT NULL ,
    `idPoslodavac` int  NOT NULL ,
    `komentar` text  NOT NULL ,
    `datumKomentiranja` date  NOT NULL ,
    PRIMARY KEY (
        `idKorisnik`,`idPoslodavac`, `datumKomentiranja`
    )
);

CREATE TABLE `Ocjenjuje` (
    `idKorisnik` int  NOT NULL ,
    `idPoslodavac` int  NOT NULL ,
    `ocjena` int  NOT NULL ,
    `datumOcjene` date  NOT NULL ,
    PRIMARY KEY (
        `idKorisnik`,`idPoslodavac`,`datumOcjene`
    )
);

CREATE TABLE `Poruka` (
    `idKorisnik` int  NOT NULL ,
    `idPoslodavac` int  NOT NULL ,
    `tekstPoruke` text  NOT NULL ,
    `vrijemeSlanja` datetime  NOT NULL ,
    PRIMARY KEY (
        `idKorisnik`,`idPoslodavac`,`vrijemeSlanja`
    )
);

CREATE TABLE `Oglas` (
    `idOglas` int  auto_increment ,
    `datumObjave` date  NOT NULL ,
    `nazivOglasa` varchar(50)  NOT NULL ,
    `tekstOglasa` text  NOT NULL ,
    `datumRoka` date ,
    `aktivan` boolean  NOT NULL ,
    `idPoslodavac` int  NOT NULL ,
    PRIMARY KEY (
        `idOglas`,`datumObjave`
    )
);

CREATE TABLE `KorisnikSpremaOglas` (
    `idKorisnik` int  NOT NULL ,
    `idOglas` int  NOT NULL ,
    PRIMARY KEY (
        `idKorisnik`,`idOglas`
    )
);

CREATE TABLE `KorisnikPrijavljujeOglas` (
    `idKorisnik` int  NOT NULL ,
    `idOglas` int  NOT NULL ,
    `datumPrijave` int  NOT NULL ,
    PRIMARY KEY (
        `idKorisnik`,`idOglas`,`datumPrijave`
    )
);

CREATE TABLE `OglasTraziZnanja` (
    `idOglas` int  NOT NULL ,
    `idZnanje` int  NOT NULL ,
    PRIMARY KEY (
        `idOglas`,`idZnanje`
    )
);

ALTER TABLE `Korisnik` ADD CONSTRAINT `fk_Korisnik_idStupanj` FOREIGN KEY(`idStupanj`)
REFERENCES `StupanjObrazovanja` (`idStupanj`);

ALTER TABLE `KorisnikRadiUFirmi` ADD CONSTRAINT `fk_KorisnikRadiUFirmi_idKorisnik` FOREIGN KEY(`idKorisnik`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `KorisnikRadiUFirmi` ADD CONSTRAINT `fk_KorisnikRadiUFirmi_idFirma` FOREIGN KEY(`idFirma`)
REFERENCES `Firma` (`idFirma`);

ALTER TABLE `Znanja` ADD CONSTRAINT `fk_Znanja_idKategorija` FOREIGN KEY(`idKategorija`)
REFERENCES `Kategorija` (`idKategorija`);

ALTER TABLE `KorisnikZnanja` ADD CONSTRAINT `fk_KorisnikZnanja_idKorisnik` FOREIGN KEY(`idKorisnik`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `KorisnikZnanja` ADD CONSTRAINT `fk_KorisnikZnanja_idZnanje` FOREIGN KEY(`idZnanje`)
REFERENCES `Znanja` (`idZnanje`);

ALTER TABLE `Komentira` ADD CONSTRAINT `fk_Komentira_idKorisnik` FOREIGN KEY(`idKorisnik`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `Komentira` ADD CONSTRAINT `fk_Komentira_idPoslodavac` FOREIGN KEY(`idPoslodavac`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `Ocjenjuje` ADD CONSTRAINT `fk_Ocjenjuje_idKorisnik` FOREIGN KEY(`idKorisnik`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `Ocjenjuje` ADD CONSTRAINT `fk_Ocjenjuje_idPoslodavac` FOREIGN KEY(`idPoslodavac`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `Poruka` ADD CONSTRAINT `fk_Poruka_idKorisnik` FOREIGN KEY(`idKorisnik`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `Poruka` ADD CONSTRAINT `fk_Poruka_idPoslodavac` FOREIGN KEY(`idPoslodavac`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `Oglas` ADD CONSTRAINT `fk_Oglas_idPoslodavac` FOREIGN KEY(`idPoslodavac`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `KorisnikSpremaOglas` ADD CONSTRAINT `fk_KorisnikSpremaOglas_idKorisnik` FOREIGN KEY(`idKorisnik`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `KorisnikSpremaOglas` ADD CONSTRAINT `fk_KorisnikSpremaOglas_idOglas` FOREIGN KEY(`idOglas`)
REFERENCES `Oglas` (`idOglas`);

ALTER TABLE `KorisnikPrijavljujeOglas` ADD CONSTRAINT `fk_KorisnikPrijavljujeOglas_idKorisnik` FOREIGN KEY(`idKorisnik`)
REFERENCES `Korisnik` (`idKorisnik`);

ALTER TABLE `KorisnikPrijavljujeOglas` ADD CONSTRAINT `fk_KorisnikPrijavljujeOglas_idOglas` FOREIGN KEY(`idOglas`)
REFERENCES `Oglas` (`idOglas`);

ALTER TABLE `OglasTraziZnanja` ADD CONSTRAINT `fk_OglasTraziZnanja_idOglas` FOREIGN KEY(`idOglas`)
REFERENCES `Oglas` (`idOglas`);

ALTER TABLE `OglasTraziZnanja` ADD CONSTRAINT `fk_OglasTraziZnanja_idZnanje` FOREIGN KEY(`idZnanje`)
REFERENCES `Znanja` (`idZnanje`);


import { Korisnik } from './korisnik.model';

export class StupanjObrazovanja{
    idStupanj: number;
    imeStupanj: string;
    korisnici: Korisnik[];
}
export class KorisnikPrijavljujeOglas{
    idKorisnik: number;
    idOglas: number;
    datumPrijave: Date;
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { SignupComponent } from '../signup/signup.component';
import { UsersignupComponent } from '../usersignup/usersignup.component';
import { EmployersignupComponent } from '../employersignup/employersignup.component';
import { ProfileComponent } from '../profile/profile.component';
import { EmployerprofileComponent } from '../employerprofile/employerprofile.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'signuppage', component: SignupComponent },
  { path: 'usersignuppage', component: UsersignupComponent },
  { path: 'employersignuppage', component: EmployersignupComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'employerprofile', component: EmployerprofileComponent }
 
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }

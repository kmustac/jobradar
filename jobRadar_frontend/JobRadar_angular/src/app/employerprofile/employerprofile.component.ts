import { Component, OnInit, Inject } from '@angular/core';
import { Korisnik } from '../korisnik.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { StupanjObrazovanja } from '../stupanjobrazovanja.model';
import { getLocaleWeekEndRange } from '@angular/common';
import { Kategorija } from '../kategorija.model';
import { Znanja } from '../znanja.model';
import { KorisnikService } from '../korisnik.service';
import { Firma } from '../firma.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Oglas } from '../oglas.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface DialogData {
  oglas: Oglas;
  firma: Firma;
}

@Component({
  selector: 'app-employerprofile',
  templateUrl: './employerprofile.component.html',
  styleUrls: ['./employerprofile.component.css']
})
export class EmployerprofileComponent implements OnInit {

  user: Korisnik = new Korisnik();
  objavljeniOglasi: Oglas[];
  firma: Firma = new Firma();
  
  baremJednaPromjena: boolean = false;

  file: File;

  isHovering: boolean = false;

  noviOglasFG: FormGroup;
  noviOglas: Oglas = new Oglas();

  filteredZnanja: Observable<Znanja[]>;
  allZnanja: Znanja[];
  odabranaZnanja: Znanja[] = new Array<Znanja>();

  oglasSearchTerm: String;

  constructor(private http: HttpClient, private router: Router, private korisnikService: KorisnikService, 
    public dialog: MatDialog) { }

  ngOnInit() {
    this.getKorisnik();
    this.noviOglasFG = new FormGroup(
      {
        nazivOglasa: new FormControl('', [Validators.required]),
        datumRoka: new FormControl('', [Validators.required]),
        tekstOglasa: new FormControl('', [Validators.required]),
        znanjaOglasa: new FormControl('', [])
      }
    )
    this.filteredZnanja = this.noviOglasFG.get('znanjaOglasa').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): Znanja[]{
    if(value && this.allZnanja && (typeof value == "string")){
      const filterValue = value.toLowerCase();
      return this.allZnanja.filter(znanje => znanje.imeZnanje.toLowerCase().includes(filterValue));
    }
  }

  addZnanje(znanje: Znanja){
    this.odabranaZnanja.push(znanje);
  }

  removeZnanje(znanje: Znanja){
    var index = this.odabranaZnanja.indexOf(znanje, 0);
    if (index > -1){
      this.odabranaZnanja.splice(index, 1);
    }
  }

  getKorisnik(){
    if(localStorage.getItem('username')){
      this.http.get('http://localhost:8080/korisnik/get/' + localStorage.getItem('username')).subscribe(
        (data: Korisnik) => {
          this.user = data;
          this.objavljeniOglasi = this.user.objavljeniOglasi;
          this.http.get('http://localhost:8080/korisnik/getFirma/' + this.user.idKorisnik).subscribe(
            (data: Firma) => {
              this.firma = data;
              this.http.get('http://localhost:8080/znanja').subscribe(
                (data: Znanja[]) =>{
                  this.allZnanja = data;
                }
              )
            }
          )
        }
      );
    }else{
      this.router.navigate(['']);
    }
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.noviOglasFG.controls[controlName].hasError(errorName);
  }


  fileChange(event){
    if(event.target.files && event.target.files[0]){
      const reader = new  FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event1) => {
        (document.getElementById('profileImage') as HTMLImageElement).src = (<FileReader>event1.target).result.toString();
      };
      
      this.file = event.target.files[0];
      this.upload();
    }
  }

  upload(){
    this.postFile(this.file).subscribe((resp: String) => {
      console.log(resp);
      console.log("nije error");
    }, error => {
      console.log(error);
      console.log("error jeste");
    });
  }

  postFile(fileToUpload: File){
    const endpoint = "http://localhost:8080/korisnik/addPic";
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, this.user.emailKorisnik + ".png");
    this.korisnikService.profilePic.next(this.user.emailKorisnik + ".png");
    if(this.file != null){
      return this.http.post(endpoint, formData);
    }
  }

  mouseHovering(){
    this.isHovering = true;
  }

  mouseLeft(){
    this.isHovering = false;
  }

  onSubmit(){
    this.noviOglas.nazivOglasa = this.noviOglasFG.get('nazivOglasa').value;
    this.noviOglas.tekstOglasa = this.noviOglasFG.get('tekstOglasa').value;
    this.noviOglas.znanja = this.odabranaZnanja;
    this.noviOglas.aktivan = true;
    this.noviOglas.datumObjave = new Date();
    this.noviOglas.poslodavac = this.user;
    this.noviOglas.datumRoka = this.noviOglasFG.get('datumRoka').value;

    this.http.post('http://localhost:8080/oglas/add', this.noviOglas).subscribe(
      (data: Oglas) => {
        this.resetForm();
        this.objavljeniOglasi.push(data);
      }
    )
  }
  
  resetForm(){
    this.noviOglasFG.get('nazivOglasa').reset();
    this.noviOglasFG.get('datumRoka').reset();
    this.noviOglasFG.get('tekstOglasa').reset();
  }

  openOglas(oglas: Oglas): void{
    const dialogRef = this.dialog.open(OglasDialog, {
      width: '80%',
      data: {oglas: oglas, firma: this.firma }
    });
  }
}


@Component({
  selector: 'app-oglasdialog',
  templateUrl: 'oglasDialog.html',
  styleUrls: ['./oglasDialog.css']
})
export class OglasDialog {

  oglasData: Oglas;
  firmaData: Firma;
  korisnici: Korisnik[] = new Array<Korisnik>();

  constructor(
    public dialogRef: MatDialogRef<OglasDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ){
    this.oglasData = data.oglas;
    this.firmaData = data.firma;
  }

  ngOnInit(){
    this.http.get('http://localhost:8080/oglas/getKorisnici/' + this.oglasData.idOglas).subscribe(
      (data: Korisnik[]) => {
        this.korisnici = data;
        console.log(this.korisnici)
      }
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
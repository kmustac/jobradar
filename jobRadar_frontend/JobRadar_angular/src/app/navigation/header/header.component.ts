import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Korisnik} from 'src/app/korisnik.model';
import { HttpClient } from '@angular/common/http';
import { KorisnikService } from '../../korisnik.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();

  /* Error handling */
  existingEmailError: boolean = false;
  wrongPasswordError: boolean = false;

  /* Toggles */
  loginToggle: boolean = false;
  logiran: boolean = false;

  /* Form Controls */
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  passFormControl = new FormControl('', [
    Validators.required
  ]);

  /* Models */
  korisnik: Korisnik = new Korisnik();
  

  constructor(private http: HttpClient, private korisnikSer: KorisnikService) { 
    this.korisnikSer.isUserLoggedIn.subscribe(
      value => {
        this.logiran = value;
      }
    );
  }

  ngOnInit() {
    if(localStorage.getItem('username')){
      this.korisnikSer.isUserLoggedIn.next(true);
    }
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  public toggleLogin(){
    this.loginToggle = !this.loginToggle;
    this.emailFormControl.reset();
    this.passFormControl.reset();
  }

  login(){
    var email = this.emailFormControl.value;
    var pass = this.passFormControl.value;
    console.log(email, pass)
    this.http.get('http://localhost:8080/korisnik/get/' + email).subscribe(
      (data: Korisnik) => {
        if(data != null){
          if(pass == data.lozinkaKorisnik){
            localStorage.setItem('username', email);
            this.korisnikSer.isUserLoggedIn.next(true);
            this.toggleLogin();
          }else{
            console.log("Krivi pass")
            this.wrongPasswordError = true;
            this.korisnikSer.isUserLoggedIn.next(false);
          }
        }else{
          this.existingEmailError = true;
          this.korisnikSer.isUserLoggedIn.next(false);
        }
      }
    )
  }


}

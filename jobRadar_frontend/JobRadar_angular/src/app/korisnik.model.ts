import { StupanjObrazovanja } from './stupanjobrazovanja.model';
import { Znanja } from './znanja.model';
import { Oglas } from './oglas.model';

export class Korisnik{
    idKorisnik: number;
    imeKorisnik: string;
    prezimeKorisnik: string;
    emailKorisnik: string;
    lozinkaKorisnik: string;
    putDoProfilne: string;
    putDoCV: string;
    jeAdmin: boolean;
    stupanjObrazovanja: StupanjObrazovanja;
    znanja: Znanja[];
    spremljeniOglasi: Oglas[];
    objavljeniOglasi: Oglas[];
}
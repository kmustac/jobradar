import { Korisnik } from './korisnik.model';
import { Firma } from './firma.model';

export class KorisnikRadiUFirmi{
    idKorisnik: number;
    idFirma: number;
    datumRada: Date;
    korisnikZaposlenik: Korisnik;
    firma: Firma;
}
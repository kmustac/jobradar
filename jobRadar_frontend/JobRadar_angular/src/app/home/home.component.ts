import { Component, OnInit } from '@angular/core';
import { Korisnik } from '../korisnik.model';
import { HttpClient } from '@angular/common/http';
import { Oglas } from '../oglas.model';
import { FormControl } from '@angular/forms';
import { OglasDialogProfile } from '../profile/profile.component';
import { MatDialog } from '@angular/material';
import { KorisnikPrijavljujeOglas } from '../korisnikPrijavljujeOglas.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: Korisnik;
  sviOglasi: Oglas[];
  searchOglasi: Oglas[];
  page: number = 1;
  pageSize: number = 10;
  searchTerm: FormControl = new FormControl();
  searchFilter: string = "naziv";
  kpo: KorisnikPrijavljujeOglas = new KorisnikPrijavljujeOglas();

  constructor(private http: HttpClient, public dialog: MatDialog) { }

  ngOnInit() {
    this.http.get("http://localhost:8080/korisnik/get/" + localStorage.getItem('username')).subscribe(
      (data: Korisnik) => {
        this.user = data
        this.http.get("http://localhost:8080/oglas/getAllOglasiOfUser/" + data.idKorisnik).subscribe(
          (data: Oglas[]) => {
            this.sviOglasi = data;

            this.sviOglasi.forEach(oglas =>{
              this.http.get("http://localhost:8080/oglas/getLogoFirme/" + oglas.idOglas, {responseType: 'text'}).subscribe(
                (data2: String)=>{
                  oglas.putDoLogoFirma = data2;
                  console.log(oglas.putDoLogoFirma);
                }
              )
            });
            this.searchOglasi = this.sviOglasi;
          }
        )
      }
    )
  }

  search(){
    console.log(this.searchFilter)
    if(this.searchTerm.value == "" || this.searchTerm.value == undefined){
      this.searchOglasi = this.sviOglasi;
    }else{
      this.searchOglasi = this.sviOglasi;
      var pomoc: Oglas[] = new Array<Oglas>();
      if(this.searchFilter == "naziv"){
        this.searchOglasi.forEach(oglas => {
          if(oglas.nazivOglasa.toLowerCase().includes(this.searchTerm.value.toLowerCase())){
            pomoc.push(oglas);
          }      
        });
        this.searchOglasi = pomoc;
      }else if(this.searchFilter == "text"){
        this.searchOglasi.forEach(oglas =>{
          if(oglas.tekstOglasa.toLowerCase().includes(this.searchTerm.value.toLowerCase())){
            pomoc.push(oglas);
          }
        });
        this.searchOglasi = pomoc;
      }else if(this.searchFilter == "vjestine"){
        this.searchOglasi.forEach(oglas => {
          oglas.znanja.forEach(znanje => {
            if(znanje.imeZnanje.toLowerCase().includes(this.searchTerm.value.toLowerCase())){
              pomoc.push(oglas);
            }
          });
        });
      }
    }
  }

  openOglas(oglas:Oglas){
    const dialogRef = this.dialog.open(OglasDialogProfile, {
      width: '80%',
      data: {userData: this.user, oglas: oglas, tipOglasa: "Spremljeni"}
    });
  }

  prijaviOglas(oglasData: Oglas){
    this.kpo.idKorisnik = this.user.idKorisnik;
    this.kpo.idOglas = oglasData.idOglas;
    this.kpo.datumPrijave = new Date();
    this.http.post("http://localhost:8080/korisnik/prijaviOglas/",this.kpo).subscribe(
      (data: any)=>{
        this.http.post("http://localhost:8080/oglas/removeFromSpremljeni/", {
          "idKorisnik":this.user.idKorisnik,
          "idOglas": oglasData.idOglas
        }, {responseType: "text"}).subscribe(
          (data: any) => {
            if(data){
              window.alert("Uspješno ste se prijavili na oglas")
              window.location.reload()
            }else{
              window.alert("Prijava na oglas nije moguća")
            }
          }
        )
      }
    )
  }

  spremiOglas(oglas: Oglas){
    this.http.post("http://localhost:8080/oglas/addToSpremljeni/", {
      "idKorisnik" : this.user.idKorisnik,
      "idOglas": oglas.idOglas
    }, {responseType: "text"}).subscribe(
      (data: any) =>{
        if(data){
          window.alert("Oglas je uspješno spremljen")
        }else{
          window.alert("Oglas nije uspješno spremljen")
        }
      }
    )
  }

  public executeSelectedChange = (event) => {
    console.log(event);
  }

}

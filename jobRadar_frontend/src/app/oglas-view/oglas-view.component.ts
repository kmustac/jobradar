import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Oglas } from '../oglas.model';
import { Firma } from '../firma.model';
import { Korisnik } from '../korisnik.model';
import { KorisnikRadiUFirmi } from '../korisnikRadiUFirmi.model';
import { KorisnikService } from '../korisnik.service';
import {Location} from '@angular/common';
import { KorisnikPrijavljujeOglas } from '../korisnikPrijavljujeOglas.model';

@Component({
  selector: 'app-oglas-view',
  templateUrl: './oglas-view.component.html',
  styleUrls: ['./oglas-view.component.css']
})
export class OglasViewComponent implements OnInit {

  oglasId: number;
  oglas: Oglas = new Oglas();
  firmaOglas: Firma = new Firma();
  user: Korisnik = new Korisnik();
  isEmployer: boolean = false;
  korisnici: Korisnik[] = new Array<Korisnik>();
  tipOglasa: string = "Spremljeni";
  prosloVrijeme: boolean = false;
  isMine: boolean = false;

  constructor(private router: Router, private http: HttpClient, private korisnikService: KorisnikService, private _location: Location) { 
    this.korisnikService.tipOglasa.subscribe(
      value => {
        this.tipOglasa = value;
      }
    );
  }

  ngOnInit() {
    this.oglasId = +this.router.url.replace('/oglas/', '');
    this.http.get('http://localhost:8080/oglas/get/' + this.oglasId).subscribe(
      (data: Oglas) => {
        this.oglas = data;

        var dateRok = new Date(this.oglas.datumRoka);
        var date = new Date();
        if (dateRok.getTime() > date.getTime()){
          this.prosloVrijeme = false;
        }else{
          this.prosloVrijeme = true;
        }

        this.http.get('http://localhost:8080/oglas/getFirmaOglas/' + this.oglas.idOglas).subscribe(
          (data: Firma) => {
            this.firmaOglas = data;
            this.http.get('http://localhost:8080/korisnik/get/' + localStorage.getItem('username')).subscribe(
              (dataK: Korisnik) => {
                this.user = dataK;
                this.http.get('http://localhost:8080/korisnik/isEmployer/' + dataK.idKorisnik).subscribe(
                  (data: KorisnikRadiUFirmi) => {
                    if(data){
                      this.isEmployer = true;
                      
                      this.http.get('http://localhost:8080/oglas/getPoslodavac/' + this.oglas.idOglas).subscribe(
                        (data: Korisnik) => {
                          if(data.idKorisnik == dataK.idKorisnik){
                            this.isMine = true;
                          }else{
                            this.isMine = false;
                          }
                        }
                      )
                    }else{
                      this.isEmployer = false;
                    }
                  }
                )
              }
            )
          }
        )
      }
    );
    this.http.get('http://localhost:8080/oglas/getKorisnici/' + this.oglas.idOglas).subscribe(
      (data: Korisnik[]) => {
        this.korisnici = data;
      }
    );
  }

  backClicked(){
    this._location.back();
  }

  prijaviOglas(){
    var kpo: KorisnikPrijavljujeOglas = new KorisnikPrijavljujeOglas();

    kpo.idKorisnik = this.user.idKorisnik;
    kpo.idOglas = this.oglas.idOglas;
    kpo.datumPrijave = new Date();

    this.http.post("http://localhost:8080/korisnik/prijaviOglas/",kpo).subscribe(
      (data: any)=>{
        this.http.post("http://localhost:8080/oglas/removeFromSpremljeni/", {
          "idKorisnik":this.user.idKorisnik,
          "idOglas": this.oglas.idOglas
        }, {responseType: "text"}).subscribe(
          (data: any) => {
            if(data){
              window.alert("Uspješno ste se prijavili na oglas")
              window.location.reload()
            }else{
              window.alert("Prijava na oglas nije moguća")
            }
          }
        )
      }
    )
  }

  spremiOglas(){
    this.http.post("http://localhost:8080/oglas/addToSpremljeni/", {
      "idKorisnik" : this.user.idKorisnik,
      "idOglas": this.oglas.idOglas
    }, {responseType: "text"}).subscribe(
      (data: any) =>{
        if(data){
          window.alert("Oglas je uspješno spremljen")
        }else{
          window.alert("Oglas nije uspješno spremljen")
        }
      }
    )
  }

  removeFromSpremljeni(){
    this.http.post("http://localhost:8080/oglas/removeFromSpremljeni/", {
          "idKorisnik":this.user.idKorisnik,
          "idOglas": this.oglas.idOglas
        }, {responseType: "text"}).subscribe(
          (data: any) => {
            if(data){
              window.alert("Uspješno ste spremili oglas")
              window.location.reload()
            }else{
              window.alert("Nije moguće spremiti oglas")
            }
          }
        );
  }

  routeToFirma(){
    this.router.navigate(['/empView', this.firmaOglas.idFirma]);
  }

}

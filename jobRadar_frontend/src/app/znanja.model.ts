import { Oglas } from './oglas.model';
import { Korisnik } from './korisnik.model';
import { Kategorija } from './kategorija.model';

export class Znanja{
    idZnanje: number;
    imeZnanje: string;
    kategorija: Kategorija;
    korisnici: Korisnik[];
    oglasi: Oglas[]
}
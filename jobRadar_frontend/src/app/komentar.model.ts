import { Korisnik } from './korisnik.model';

export class Komentar{
    korisnik: Korisnik;
    datumKomentara: Date;
    tekstKomentara: string;
}
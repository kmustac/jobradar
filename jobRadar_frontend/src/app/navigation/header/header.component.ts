import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {Korisnik} from 'src/app/korisnik.model';
import { HttpClient } from '@angular/common/http';
import { KorisnikService } from '../../korisnik.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();

  /* Error handling */
  existingEmailError: boolean = false;
  wrongPasswordError: boolean = false;

  /* Toggles */
  loginToggle: boolean = false;
  logiran: boolean = false;

  /* Form Controls */
  loginFG: FormGroup;

  /* Models */
  korisnik: Korisnik = new Korisnik();
  

  constructor(private http: HttpClient, private korisnikSer: KorisnikService) { 
    this.korisnikSer.isUserLoggedIn.subscribe(
      value => {
        this.logiran = value;
      }
    );
  }

  ngOnInit() {
    if(localStorage.getItem('username')){
      this.korisnikSer.isUserLoggedIn.next(true);
    }
    if(!this.logiran){
      this.loginFG = new FormGroup(
        {
          emailKorisnik: new FormControl('', [Validators.required, Validators.email]),
          lozinkaKorisnik: new FormControl('', [Validators.required])
        }
      )
    }
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  public toggleLogin(){
    this.loginToggle = !this.loginToggle;
    this.loginFG.reset();
  }

  login(){
    var email = this.loginFG.get('emailKorisnik').value;
    var pass = this.loginFG.get('lozinkaKorisnik').value;
    console.log(email, pass)
    this.http.get('http://localhost:8080/korisnik/get/' + email).subscribe(
      (data: Korisnik) => {
        if(data != null){
          if(pass == data.lozinkaKorisnik){
            localStorage.setItem('username', email);
            this.korisnikSer.isUserLoggedIn.next(true);
            this.toggleLogin();
            window.location.reload()
          }else{
            this.loginFG.controls['lozinkaKorisnik'].setErrors({'incorrect': true})
            this.korisnikSer.isUserLoggedIn.next(false);
          }
        }else{
          this.loginFG.controls['emailKorisnik'].setErrors({'incorrect': true})
          this.korisnikSer.isUserLoggedIn.next(false);
        }
      }
    )
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.loginFG.controls[controlName].hasError(errorName);
  }


}

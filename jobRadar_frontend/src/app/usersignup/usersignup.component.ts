import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Korisnik } from '../korisnik.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { resolve } from 'q';
import { KorisnikService } from '../korisnik.service';

@Component({
  selector: 'app-usersignup',
  templateUrl: './usersignup.component.html',
  styleUrls: ['./usersignup.component.css']
})
export class UsersignupComponent implements OnInit {

  userForm: FormGroup;
  newKorisnik: Korisnik = new Korisnik();

  constructor(private http: HttpClient, private router: Router, private korisnikSer: KorisnikService) { }

  ngOnInit() {
    this.userForm = new FormGroup(
      {
        imeKorisnik: new FormControl('', [Validators.required]),
        prezimeKorisnik: new FormControl('', [Validators.required]),
        emailKorisnik: new FormControl('', [Validators.required, Validators.email, this.validateExistingEmail.bind(this)]),
        lozinkaKorisnik: new FormControl('', [Validators.required, Validators.minLength(6)]),
        potvrdaLozinkaKorisnik: new FormControl('', [Validators.required, this.validatePasswordMatch.bind(this)])
      }
    )
  }
  public hasError = (controlName: string, errorName: string) =>{
    return this.userForm.controls[controlName].hasError(errorName);
  }

  validatePasswordMatch(){
    if(this.userForm){
      return this.userForm.get('lozinkaKorisnik').value == this.userForm.get('potvrdaLozinkaKorisnik').value ? null: { PasswordMatch: false} 
    }
    return { PasswordMatch: false};
  }

  validateExistingEmail(){
    if(this.userForm && this.userForm.get('emailKorisnik').value != ""){
      this.http.get('http://localhost:8080/korisnik/get/' + this.userForm.get('emailKorisnik').value).subscribe(
        (data: Korisnik) => {
          if(data){
            this.userForm.controls['emailKorisnik'].setErrors({'incorrect': true});
          }
        }
      )
    }
  }

  onSubmit(){
    this.newKorisnik.imeKorisnik = this.userForm.get('imeKorisnik').value;
    this.newKorisnik.prezimeKorisnik = this.userForm.get('prezimeKorisnik').value;
    this.newKorisnik.emailKorisnik = this.userForm.get('emailKorisnik').value;
    this.newKorisnik.lozinkaKorisnik = this.userForm.get('lozinkaKorisnik').value;

    this.http.post('http://localhost:8080/korisnik/add', this.newKorisnik).subscribe(
      (data: Korisnik) => {
        if(data != null){
          localStorage.setItem('username', data.emailKorisnik);
          this.korisnikSer.isUserLoggedIn.next(true);
          this.router.navigate(['']);
        }else{
          alert('Neki error!')
        }
      }
    )
  }

}

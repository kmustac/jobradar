import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KorisnikService } from '../korisnik.service';
import { Router } from '@angular/router';
import { Firma } from '../firma.model';
import { Oglas } from '../oglas.model';
import { Korisnik } from '../korisnik.model';
import { Komentira } from '../komentira.module';
import { Komentar } from '../komentar.model';
import { FormControl } from '@angular/forms';
import { Ocjenjuje } from '../ocjenjuje.model';

@Component({
  selector: 'app-employer-view',
  templateUrl: './employer-view.component.html',
  styleUrls: ['./employer-view.component.css']
})
export class EmployerViewComponent implements OnInit {

  firma: Firma = new Firma();
  allOglasi: Oglas[] = new Array<Oglas>();
  isEmployer: boolean = false;
  ocjena: number = 3;
  user: Korisnik = new Korisnik();
  ocjenaFirma: number = 0;
  sviKomentari: Komentar[] = new Array<Komentar>();
  textAreaInput: FormControl = new FormControl();
  vecOcjenjen: boolean = false;
  vecKomentirao: boolean = false;
  jeLogiran: boolean = false;

  constructor(private http: HttpClient, private korisnikService: KorisnikService, private router: Router) { 
    this.korisnikService.isEmployer.subscribe(
      value => this.isEmployer = value
    )
    this.korisnikService.isUserLoggedIn.subscribe(
      value => this.jeLogiran = value
    )
  }

  ngOnInit() {
    var idFirma: number = +this.router.url.replace('/empView/', '');
    this.http.get('http://localhost:8080/firma/getById/' + idFirma).subscribe(
      (data: Firma) => {
        this.firma = data;
        
        this.http.get('http://localhost:8080/firma/getAllAktualni/'+ this.firma.idFirma).subscribe(
          (data: Oglas[]) => {
            this.allOglasi = data;
          }
        )

        this.http.get('http://localhost:8080/firma/getOcjena/' + this.firma.idFirma).subscribe(
          (data: number) => {
            this.ocjenaFirma = data;
          }
        )

        this.http.get('http://localhost:8080/firma/getKomentari/' + this.firma.idFirma).subscribe(
          (data: Komentira[]) => {
            var pomoc: Komentar[] = new Array<Komentar>();
            data.forEach(
              value => {
                this.http.get('http://localhost:8080/korisnik/getById/' + value.idKorisnik).subscribe(
                  (datak: Korisnik) => {
                    var kom: Komentar = new Komentar();
                    kom.korisnik = datak;
                    kom.datumKomentara = value.datumKomentiranja;
                    kom.tekstKomentara = value.komentar;
                    pomoc.push(kom);
                  }
                )
              }
            )
            this.sviKomentari = pomoc;
          }
        )

        this.http.get('http://localhost:8080/korisnik/get/' + localStorage.getItem('username')).subscribe(
          (data: Korisnik) => {
            this.user = data;
            if(!this.isEmployer){
              var o: Ocjenjuje = new Ocjenjuje();
              o.idKorisnik = this.user.idKorisnik;
              o.idFirma = this.firma.idFirma
              this.http.post('http://localhost:8080/korisnik/jeLiOcjenio/', o).subscribe(
                (data: Ocjenjuje) => {
                  if(data){
                    this.vecOcjenjen = true;
                    this.ocjena = data.ocjena;
                  }else{
                    this.vecOcjenjen = false;
                  }
                }
              )
              var k: Komentira = new Komentira();
              k.idKorisnik = this.user.idKorisnik;
              k.idFirma = this.firma.idFirma;
              this.http.post('http://localhost:8080/korisnik/jeKomentirao/', k).subscribe(
                (data: Komentira) => {
                  if(data){
                    this.vecKomentirao = true;
                  }else{
                    this.vecKomentirao = false;
                  }
                }
              )
            }
          }
        )
      }
    )
  }

  ocjeni(){
    if(confirm("Zelite li stvarno dati ovu ocjenu?")){
      var o: Ocjenjuje = new Ocjenjuje();
      o.idFirma = this.firma.idFirma;
      o.idKorisnik = this.user.idKorisnik;
      o.ocjena = this.ocjena;
      o.datumOcjene = new Date();
      
      this.http.post('http://localhost:8080/korisnik/dodajOcjenu/', o).subscribe(
        (data: Ocjenjuje) => {
          if(data){
            window.alert("Ocjena je uspješno pohranjena!");
            window.location.reload();
          }else{
            window.alert("Error-ocjenjivanje");
          }
        }
      )
    }
  }

  noComment(){
    this.textAreaInput.reset();
  }

  publishComment(){
    var tekst: string = this.textAreaInput.value;
    var komentira: Komentira = new Komentira();
    komentira.idKorisnik = this.user.idKorisnik;
    komentira.idFirma = this.firma.idFirma;
    komentira.datumKomentiranja = new Date();
    komentira.komentar = tekst;

    if(confirm("Zelite li stvarno dodati objaviti ovaj komentar?")){
      this.http.post('http://localhost:8080/korisnik/dodajKomentar/', komentira).subscribe(
        (data: Komentira) => {
          if(data){
            window.alert("Komentar je uspješno objavljen!")
            window.location.reload()
          }else{
            window.alert("Error-komentar")
          }
        }
      )
    }
  }
}

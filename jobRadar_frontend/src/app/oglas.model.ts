import { Korisnik } from './korisnik.model';
import { Znanja } from './znanja.model';

export class Oglas{
    idOglas: number;
    datumObjave: Date;
    nazivOglasa: string;
    tekstOglasa: string;
    datumRoka: Date;
    aktivan: boolean;
    inEditing: boolean;
    poslodavac: Korisnik;
    znanja: Znanja[];
    korisnici: Korisnik[];
    putDoLogoFirma: String;
    prosaoRok: boolean;
}
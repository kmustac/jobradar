import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Korisnik } from '../korisnik.model';
import { Firma } from '../firma.model';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { KorisnikRadiUFirmi } from '../korisnikRadiUFirmi.model';
import { Router } from '@angular/router';
import { LoggedinComponent } from '../loggedin/loggedin.component';
import { KorisnikService } from '../korisnik.service';

@Component({
  selector: 'app-employersignup',
  templateUrl: './employersignup.component.html',
  styleUrls: ['./employersignup.component.css']
})
export class EmployersignupComponent implements OnInit {

  userForm: FormGroup;
  firme: Firma[];
  filteredFirme: Observable<Firma[]>;
  newKorisnik: Korisnik = new Korisnik();
  kruf: KorisnikRadiUFirmi = new KorisnikRadiUFirmi();
  constructor(private http: HttpClient, private router: Router, private korisnikSer: KorisnikService) { }

  ngOnInit() {
    this.userForm = new FormGroup(
      {
        imeKorisnik: new FormControl('', [Validators.required]),
        prezimeKorisnik: new FormControl('', [Validators.required]),
        emailKorisnik: new FormControl('', [Validators.required, Validators.email, this.validateExistingEmail.bind(this)]),
        lozinkaKorisnik: new FormControl('', [Validators.required, Validators.minLength(6)]),
        potvrdaLozinkaKorisnik: new FormControl('', [Validators.required, this.validatePasswordMatch.bind(this)]),
        firmaKorisnik: new FormControl('', [Validators.required])
      }
    );
    this.http.get('http://localhost:8080/firma/').subscribe(
      (data: Firma[]) => {
        this.firme = data;
        console.log(this.firme)
      }
    );
    this.filteredFirme = this.userForm.get('firmaKorisnik').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  onSubmit(){
    let odabranaFirma: Firma;
    this.firme.forEach(firma => {
      if(firma.imeFirma == this.userForm.get('firmaKorisnik').value){
        odabranaFirma = firma;
      }
    });

    this.newKorisnik.imeKorisnik = this.userForm.get('imeKorisnik').value;
    this.newKorisnik.prezimeKorisnik = this.userForm.get('prezimeKorisnik').value;
    this.newKorisnik.emailKorisnik = this.userForm.get('emailKorisnik').value;
    this.newKorisnik.lozinkaKorisnik = this.userForm.get('lozinkaKorisnik').value;

    this.http.post('http://localhost:8080/korisnik/add', this.newKorisnik).subscribe(
      (datak: Korisnik) => {
        if(datak){
          this.kruf.idFirma = odabranaFirma.idFirma;
          this.kruf.idKorisnik = datak.idKorisnik;
          this.kruf.datumRada = new Date();
          this.http.post('http://localhost:8080/korisnik/add/radiUFirmi', this.kruf).subscribe(
            (dataKruf: KorisnikRadiUFirmi) => {
              if(dataKruf){
                localStorage.setItem('username', datak.emailKorisnik);
                this.korisnikSer.isUserLoggedIn.next(true);
                this.router.navigate(['']);
              }
            }
          )
        }else{
          alert('Korisnik error!')
        }
      }
    )
  }

  private _filter(value: string): Firma[]{
    if(value && this.firme && (typeof value == "string")){
      const filterValue = value.toLowerCase();
      return this.firme.filter(firma => firma.imeFirma.toLowerCase().includes(filterValue));
    }
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.userForm.controls[controlName].hasError(errorName);
  }

  validatePasswordMatch(){
    if(this.userForm){
      return this.userForm.get('lozinkaKorisnik').value == this.userForm.get('potvrdaLozinkaKorisnik').value ? null: { PasswordMatch: false} 
    }
    return { PasswordMatch: false};
  }

  validateExistingEmail(){
    if(this.userForm && this.userForm.get('emailKorisnik').value != ""){
      this.http.get('http://localhost:8080/korisnik/get/' + this.userForm.get('emailKorisnik').value).subscribe(
        (data: Korisnik) => {
          if(data){
            this.userForm.controls['emailKorisnik'].setErrors({'incorrect': true});
          }
        }
      )
    }
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { RoutingModule } from './routing/routing.module';
import { HeaderComponent } from './navigation/header/header.component';
import { LoggedinComponent } from './loggedin/loggedin.component';
import { SignupComponent } from './signup/signup.component';
import { UsersignupComponent } from './usersignup/usersignup.component';
import { EmployersignupComponent } from './employersignup/employersignup.component';
import { KorisnikService } from './korisnik.service';
import { ProfileComponent, OglasDialogProfile } from './profile/profile.component';
import { EmployerprofileComponent, OglasDialog } from './employerprofile/employerprofile.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { OglasViewComponent } from './oglas-view/oglas-view.component';
import { EmployerViewComponent } from './employer-view/employer-view.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    HeaderComponent,
    LoggedinComponent,
    SignupComponent,
    UsersignupComponent,
    EmployersignupComponent,
    ProfileComponent,
    EmployerprofileComponent,
    OglasDialog,
    OglasDialogProfile,
    OglasViewComponent,
    EmployerViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    RoutingModule,
    NgxPaginationModule
  ],
  providers: [KorisnikService],
  bootstrap: [AppComponent],
  entryComponents: [OglasDialog, OglasDialogProfile]
})
export class AppModule { }

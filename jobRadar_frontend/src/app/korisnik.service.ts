import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Korisnik } from './korisnik.model';

@Injectable()
export class KorisnikService{
    public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public profileNotifs: BehaviorSubject<number> = new BehaviorSubject<number>(0);
    public profilePic: BehaviorSubject<string> = new BehaviorSubject<string>("noImg.png");
    public isEmployer: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public tipOglasa: BehaviorSubject<string> = new BehaviorSubject<string>("spremljeni");
}
export class Firma{
    idFirma: number;
    imeFirma: string;
    emailFirma: string;
    telefonFirma: string;
    putDoLogoFirma: string;
}
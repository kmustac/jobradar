import { Znanja } from './znanja.model';

export class Kategorija{
    idKategorija: number;
    imeKategorija: string;
    znanja: Znanja[];
}
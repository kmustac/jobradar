import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule, MatSidenavModule, MatToolbarModule,
MatIconModule, MatFormFieldModule, MatButtonModule, MatInputModule, MatCardModule, MatSelectModule, MatBadgeModule, MatChipsModule, MatExpansionModule, MatListModule, MatDatepicker, MatDatepickerModule, MatNativeDateModule, MatDialogModule, MatTreeModule, MatCheckboxModule, MatRadioButton, MatRadioModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatFileUploadModule } from 'angular-material-fileupload';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { OglasDialog } from '../employerprofile/employerprofile.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    HttpClientModule,
    MatCardModule,
    MatFileUploadModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatBadgeModule,
    MatChipsModule,
    MatExpansionModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatTreeModule,
    MatCheckboxModule,
    MatRadioModule
  ],
  exports: [
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    HttpClientModule,
    MatCardModule,
    MatFileUploadModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatBadgeModule,
    MatChipsModule,
    MatExpansionModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatTreeModule,
    MatCheckboxModule,
    MatRadioModule
  ]
})
export class MaterialModule { }

import { Component, OnInit, Inject } from '@angular/core';
import { Korisnik } from '../korisnik.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { StupanjObrazovanja } from '../stupanjobrazovanja.model';
import { getLocaleWeekEndRange } from '@angular/common';
import { Kategorija } from '../kategorija.model';
import { Znanja } from '../znanja.model';
import { KorisnikService } from '../korisnik.service';
import { Oglas } from '../oglas.model';
import { Firma } from '../firma.model';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { KorisnikPrijavljujeOglas } from '../korisnikPrijavljujeOglas.model'

export interface DialogData {
  userData: Korisnik;
  oglas: Oglas;
  tipOglasa: String;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: Korisnik = new Korisnik();
  stupnjeviObrazovanja: StupanjObrazovanja[] = new Array<StupanjObrazovanja>();
  soHidden: boolean = true;
  allKategorije: Kategorija[] = new Array<Kategorija>();

  removeZnanja: Znanja[] = new Array<Znanja>();
  addZnanja: Znanja[] = new Array<Znanja>();

  baremJednaPromjena: boolean = false;

  spremljeniOglasi: Oglas[] = new Array<Oglas>();
  prijavljeniOglasi: Oglas[] = new Array<Oglas>();

  file: File;

  constructor(private http: HttpClient, private router: Router, private korisnikService: KorisnikService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getKorisnik();
    
    //dohvacanje spremljenih oglasa
    this.http.get("http://localhost:8080/korisnik/getSpremljeniOglasi/" + localStorage.getItem('username')).subscribe(
      (data: Oglas[]) =>{
        this.spremljeniOglasi = data;

        this.spremljeniOglasi.forEach(oglas => {
          this.http.get("http://localhost:8080/oglas/getLogoFirme/" + oglas.idOglas, {responseType: 'text'}).subscribe(
            (data2: String)=>{
              oglas.putDoLogoFirma = data2;
            }
          )
        });
      }
    )

    //dohvacanje prijavljenih oglasa
    this.http.get("http://localhost:8080/korisnik/getPrijavljeniOglasi/" + localStorage.getItem('username')).subscribe(
      (data: Oglas[]) => {
        this.prijavljeniOglasi = data;

        this.prijavljeniOglasi.forEach(oglas =>{
          var dateDanas = new Date();
          var dateRok = new Date(oglas.datumRoka);
          if(dateRok.getTime() > dateDanas.getTime()){
            oglas.prosaoRok = false;
          }else{
            oglas.prosaoRok = true;
          }
          this.http.get("http://localhost:8080/oglas/getLogoFirme/" + oglas.idOglas, {responseType: 'text'}).subscribe(
            (data2: String)=>{
              oglas.putDoLogoFirma = data2;
              console.log(oglas.putDoLogoFirma);
            }
          )
        });
      }
    )
  }

  getKorisnik(){
    console.log(localStorage.getItem('username'));
    if(localStorage.getItem('username')){
      this.http.get('http://localhost:8080/korisnik/get/' + localStorage.getItem('username')).subscribe(
        (data: Korisnik) => {
          this.user = data;
          this.http.get('http://localhost:8080/stupanjObr').subscribe(
            (data: StupanjObrazovanja[]) =>{
              this.stupnjeviObrazovanja = data;
              this.http.get('http://localhost:8080/kategorija').subscribe(
                (data: Kategorija[]) => {
                  this.allKategorije = data;
                }
              )
            }
          )
        }
      )
    }else{
      this.router.navigate(['']);
    }
  }

  changeSO(){
    this.soHidden = false;
  }
  revertSO(){
    this.soHidden = true;
  }

  changeStupanj(stupanj: StupanjObrazovanja){
    this.soHidden = true;
    this.http.post('http://localhost:8080/korisnik/updateStupanj', {
      'idKorisnik': this.user.idKorisnik,
      'idStupanj': stupanj.idStupanj
    }).subscribe(
      (data:any)=>{
        this.http.get('http://localhost:8080/korisnik/get/' + localStorage.getItem('username')).subscribe(
          (data: Korisnik) => {
            this.user = data;
          }
        )
      }
    )
  }

  getZnanja(kategorija: Kategorija){
    return kategorija.znanja;
  }

  isAlreadyIn(znanje: Znanja){
    const checkExistance = znanjeParam => this.user.znanja.some(({idZnanje}) => idZnanje == znanjeParam);
    return checkExistance(znanje.idZnanje);
  }

  addZnanje(znanje: Znanja){
    this.baremJednaPromjena = true;
    if(this.isAlreadyIn(znanje)){
      this.removeZnanja.push(znanje);
      const index = this.addZnanja.indexOf(znanje);
      if(index < 0)
        delete this.addZnanja[index];
    }else{
      this.addZnanja.push(znanje);
      const index = this.removeZnanja.indexOf(znanje);
      if(index < 0)
        delete this.removeZnanja[index];
    }
  }

  saveChanges(){
    this.http.post('http://localhost:8080/korisnik/modifyZnanja', {
      'idKorisnik': this.user.idKorisnik,
      'addZnanja': this.addZnanja,
      'removeZnanja': this.removeZnanja
    }).subscribe(
      (data: any) => {
        if(!data){
          this.http.get('http://localhost:8080/korisnik/get/' + localStorage.getItem('username')).subscribe(
            (data: Korisnik) => {
              this.user = data;
              this.baremJednaPromjena = false;
              this.addZnanja = new Array<Znanja>();
              this.removeZnanja = new Array<Znanja>();
            }
          )
        }else{
          window.alert("Error-znanja-modify")
        }
      }
    )
  }

  fileChangeCV(event){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      this.file = event.target.files[0];
      this.uploadCv();
    }
  }

  fileChange(event){
    if(event.target.files && event.target.files[0]){
      const reader = new  FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event1) => {
        (document.getElementById('profileImage') as HTMLImageElement).src = (<FileReader>event1.target).result.toString();
      };
      
      this.file = event.target.files[0];
      this.upload();
    }
  }

  uploadCv(){
    this.postFileCv(this.file).subscribe((resp: String) => {
      console.log(resp);
      console.log("nije error");
    }, error => {
      console.log(error);
      console.log("error jeste");
    });
  }

  upload(){
    this.postFile(this.file).subscribe((resp: String) => {
      console.log(resp);
      console.log("nije error");
      window.alert("CV uspješno uploadan!");
    }, error => {
      console.log(error);
      console.log("error jeste");
      window.alert("Error-uploadCV");
    });
  }

  postFile(fileToUpload: File){
    const endpoint = "http://localhost:8080/korisnik/addPic";
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, this.user.emailKorisnik + ".png");
    this.korisnikService.profilePic.next(this.user.emailKorisnik + ".png");
    if(this.file != null){
      return this.http.post(endpoint, formData);
    }
  }

  postFileCv(fileToUpload: File){
    const endpoint = "http://localhost:8080/korisnik/uploadPdf";
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, this.user.emailKorisnik + ".pdf");
    if(this.file != null){
      return this.http.post(endpoint, formData);
    }
  }
  
  openOglasSpremljeni(oglas: Oglas): void{
    this.korisnikService.tipOglasa.next("Spremljeni");
    this.router.navigate(["/oglas", oglas.idOglas]);
  }
  openOglasPrijavljeni(oglas: Oglas):void{
    this.korisnikService.tipOglasa.next("Prijavljeni");
    this.router.navigate(["/oglas", oglas.idOglas]);
  }

  removeOglasFromSpremljeni(oglas:Oglas){
    this.http.post("http://localhost:8080/oglas/removeFromSpremljeni/", {
      "idKorisnik":this.user.idKorisnik,
      "idOglas":oglas.idOglas
    }, {responseType: 'text'}).subscribe(
      (data:any)=>{
        if(data){
          const index = this.spremljeniOglasi.indexOf(oglas, 0);
          if (index > -1){
            this.spremljeniOglasi.splice(index, 1)
          }
        }
      }
    )
  }

  odjaviOglas(oglas: Oglas){
    var kpo: KorisnikPrijavljujeOglas = new KorisnikPrijavljujeOglas();
    kpo.idKorisnik = this.user.idKorisnik;
    kpo.idOglas = oglas.idOglas;

    this.http.post('http://localhost:8080/oglas/removeFromPrijavljeni', kpo).subscribe(
      (data: any) => {
        if(data){
          var index = this.prijavljeniOglasi.indexOf(oglas);
          if(index > -1){
            this.prijavljeniOglasi.splice(index, 1);
            window.alert("Uspjesno ste se odjavili s oglasa.")
          }
        }else{
          window.alert("error_odjava")
        }
      }
    )
  }

}


@Component({
  selector: 'app-oglasdialog',
  templateUrl: 'oglasDialog.html',
  styleUrls: ['./oglasDialog.css']
})
export class OglasDialogProfile {

  userData: Korisnik;
  oglasData: Oglas;
  tipOglasa: String;
  firmaData: Firma;
  korisnici: Korisnik[] = new Array<Korisnik>();

  kpo: KorisnikPrijavljujeOglas = new KorisnikPrijavljujeOglas();

  constructor(
    public dialogRef: MatDialogRef<OglasDialogProfile>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ){
    this.userData = data.userData;
    this.oglasData = data.oglas;
    this.tipOglasa = data.tipOglasa;
  }

  ngOnInit(){
    this.http.get("http://localhost:8080/oglas/getFirmaOglas/" + this.oglasData.idOglas).subscribe(
      (data: Firma) => {
        this.firmaData = data;
      }
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  prijaviOglas(){
    this.kpo.idKorisnik = this.userData.idKorisnik;
    this.kpo.idOglas = this.oglasData.idOglas;
    this.kpo.datumPrijave = new Date();
    this.http.post("http://localhost:8080/korisnik/prijaviOglas/",this.kpo).subscribe(
      (data: any)=>{
        this.http.post("http://localhost:8080/oglas/removeFromSpremljeni/", {
          "idKorisnik":this.userData.idKorisnik,
          "idOglas": this.oglasData.idOglas
        }, {responseType: "text"}).subscribe(
          (data: any) => {
            window.location.reload()
          }
        )
      }
    )
  }


}


import { Component, OnInit } from '@angular/core';
import { Korisnik } from '../korisnik.model';
import { HttpClient } from '@angular/common/http';
import { Oglas } from '../oglas.model';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { KorisnikPrijavljujeOglas } from '../korisnikPrijavljujeOglas.model';
import { Router } from '@angular/router';
import { KorisnikService } from '../korisnik.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: Korisnik;
  sviOglasi: Oglas[];
  apsolutnoSviOglasi: Oglas[];
  searchOglasi: Oglas[];
  page: number = 1;
  pageSize: number = 16;
  searchTerm: FormControl = new FormControl();
  searchFilter: string = "naziv";
  searchScope: string = "mojiOglasi";
  kpo: KorisnikPrijavljujeOglas = new KorisnikPrijavljujeOglas();
  sortMode: string;
  naslovSort: number = 0;
  objavaSort: number = 0;
  rokSort: number = 0;
  isUserLoggedIn: boolean;

  constructor(private http: HttpClient, public dialog: MatDialog, private router: Router, private korisnikService: KorisnikService) { 
    this.korisnikService.isUserLoggedIn.subscribe(
      value => this.isUserLoggedIn = value
    );
  }

  ngOnInit() {
    this.http.get("http://localhost:8080/oglas/getAllOglasi").subscribe(
      (data: Oglas[]) => {
        if(data){
          this.apsolutnoSviOglasi = data;
          this.apsolutnoSviOglasi.forEach(oglas =>{
            this.http.get("http://localhost:8080/oglas/getLogoFirme/" + oglas.idOglas, {responseType: 'text'}).subscribe(
              (data2: String)=>{
                oglas.putDoLogoFirma = data2;
              }
            )}
          );
          if(this.isUserLoggedIn == true){
            this.http.get("http://localhost:8080/korisnik/get/" + localStorage.getItem('username')).subscribe(
              (data: Korisnik) => {
                this.user = data
                this.http.get("http://localhost:8080/oglas/getAllOglasiOfUser/" + data.idKorisnik).subscribe(
                  (data: Oglas[]) => {
                    this.sviOglasi = data;

                    this.sviOglasi.forEach(oglas =>{
                        this.http.get("http://localhost:8080/oglas/getLogoFirme/" + oglas.idOglas, {responseType: 'text'}).subscribe(
                        (data2: String)=>{
                          oglas.putDoLogoFirma = data2;
                          }
                        );
                    });
                    this.searchScope = "mojiOglasi";
                    this.searchOglasi = this.sviOglasi;
                  }
                )
              }
            )
          }else{
            this.searchScope = "sviOglasi";
            this.searchOglasi = this.apsolutnoSviOglasi;
          }
        }else{
          window.alert("Error-home-allOglasi")
        }
      }
    )
  }

  search(){
    if(this.searchScope == "mojiOglasi"){
      this.searchOglasi = this.sviOglasi;
    }else{
      this.searchOglasi = this.apsolutnoSviOglasi;
    }
    if(this.searchTerm.value == "" || this.searchTerm.value == undefined){
    }else{
      var pomoc: Oglas[] = new Array<Oglas>();
      if(this.searchFilter == "naziv"){
        this.searchOglasi.forEach(oglas => {
          if(oglas.nazivOglasa.toLowerCase().includes(this.searchTerm.value.toLowerCase())){
            pomoc.push(oglas);
          }      
        });
      }else if(this.searchFilter == "text"){
        this.searchOglasi.forEach(oglas =>{
          if(oglas.tekstOglasa.toLowerCase().includes(this.searchTerm.value.toLowerCase())){
            pomoc.push(oglas);
          }
        });
      }else if(this.searchFilter == "vjestine"){
        this.searchOglasi.forEach(oglas => {
          oglas.znanja.forEach(znanje => {
            if(this.searchTerm.value.toLowerCase().includes(znanje.imeZnanje.toLowerCase())){
              pomoc.push(oglas);
            }
          });
        });
      }
      this.searchOglasi = pomoc;
    }
  }

  openOglas(oglas:Oglas){
    this.router.navigate(['/oglas', oglas.idOglas])
  }

  prijaviOglas(oglasData: Oglas){
    this.kpo.idKorisnik = this.user.idKorisnik;
    this.kpo.idOglas = oglasData.idOglas;
    this.kpo.datumPrijave = new Date();
    this.http.post("http://localhost:8080/korisnik/prijaviOglas/",this.kpo).subscribe(
      (data: any)=>{
        this.http.post("http://localhost:8080/oglas/removeFromSpremljeni/", {
          "idKorisnik":this.user.idKorisnik,
          "idOglas": oglasData.idOglas
        }, {responseType: "text"}).subscribe(
          (data: any) => {
            if(data){
              window.alert("Uspješno ste se prijavili na oglas")
              window.location.reload()
            }else{
              window.alert("Prijava na oglas nije moguća")
            }
          }
        )
      }
    )
  }

  spremiOglas(oglas: Oglas){
    this.http.post("http://localhost:8080/oglas/addToSpremljeni/", {
      "idKorisnik" : this.user.idKorisnik,
      "idOglas": oglas.idOglas
    }, {responseType: "text"}).subscribe(
      (data: any) =>{
        if(data){
          window.alert("Oglas je uspješno spremljen")
        }else{
          window.alert("Oglas nije uspješno spremljen")
        }
      }
    )
  }

  public executeSelectedChange = (event) => {
    console.log(event);
  }

  sort(){
    if(this.sortMode == "naslov"){
      if(this.naslovSort % 2 == 0){
        SortUtil.sortByProperty(this.searchOglasi, "nazivOglasa", "ASC");
      }else{
        SortUtil.sortByProperty(this.searchOglasi, "nazivOglasa", "DESC");
      }
      this.naslovSort += 1;
    }
    if(this.sortMode == "datumObjava"){
      if(this.objavaSort % 2 == 0){
        SortUtil.sortByDatumObjave(this.searchOglasi, "ASC");
      }else{
        SortUtil.sortByDatumObjave(this.searchOglasi,  "DESC");
      }
      this.objavaSort += 1;
    }
    if (this.sortMode == "datumRok"){
      if(this.rokSort % 2 == 0){
        SortUtil.sortByDatumRoka(this.searchOglasi, "ASC");
      }else{
        SortUtil.sortByDatumRoka(this.searchOglasi, "DESC");
      }
      this.rokSort += 1;
    }
  }

  changeScope(){
    if(this.searchScope == "mojiOglasi"){
      this.searchOglasi = this.sviOglasi;
    }else{
      this.searchOglasi = this.apsolutnoSviOglasi;
    }
  }

}



export class SortUtil {

  static sortByDatumRoka(array: Oglas[], order: 'ASC' | 'DESC'): void{
    array.sort((a, b) => {
      if(a.datumRoka.getTime < b.datumRoka.getTime){
        return -1;
      }
      if(a.datumRoka.getTime > b.datumRoka.getTime){
        return 1;
      }
      return 0;
    });
    if(order == 'DESC'){
      array.reverse();
    }
  }

  static sortByDatumObjave(array: Oglas[], order: 'ASC' | 'DESC'): void{
    array.sort((a, b) => {
      if(a.datumObjave.getTime < b.datumObjave.getTime){
        return -1;
      }
      if(a.datumObjave.getTime > b.datumObjave.getTime){
        return 1;
      }
      return 0;
    });
    if(order == 'DESC'){
      array.reverse();
    }
  }

  static sortByProperty<T>(array: T[], propName: keyof T, order: 'ASC' | 'DESC'): void {
      array.sort((a, b) => {
          if (a[propName] < b[propName]) {
              return -1;
          }

          if (a[propName] > b[propName]) {
              return 1;
          }
          return 0;
      });

      if (order === 'DESC') {
          array.reverse();
      }
    }
}

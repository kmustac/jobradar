export class Komentira{
    idKorisnik: number;
    idFirma: number;
    datumKomentiranja: Date;
    komentar: string;
}
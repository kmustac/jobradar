import { Component, OnInit } from '@angular/core';
import { Korisnik } from '../korisnik.model';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { KorisnikService } from '../korisnik.service';
import { KorisnikRadiUFirmi } from '../korisnikRadiUFirmi.model';

@Component({
  selector: 'app-loggedin',
  templateUrl: './loggedin.component.html',
  styleUrls: ['./loggedin.component.css']
})
export class LoggedinComponent implements OnInit {

  korisnik: Korisnik = new Korisnik();
  numNotifs: number;
  isEmployer: boolean = false;
  constructor(private http: HttpClient, private _sanitizer: DomSanitizer, private korisnikService: KorisnikService) { }

  ngOnInit() {
    this.http.get('http://localhost:8080/korisnik/get/'+localStorage.getItem('username')).subscribe(
      (data: Korisnik) => {
        this.korisnik = data;
        this.http.get('http://localhost:8080/korisnik/isEmployer/' + this.korisnik.idKorisnik).subscribe(
          (data: KorisnikRadiUFirmi) => {
            console.log(data);
            if(data){
              this.korisnikService.isEmployer.next(true);
            }else{
              this.korisnikService.isEmployer.next(false);
            }
          }
        )
      }
    )
    this.korisnikService.profileNotifs.subscribe(
      (data:any) => {
        this.numNotifs = data;
      }
    )
    
    this.korisnikService.profilePic.subscribe(
      (data:any) => {
        this.korisnik.putDoProfilne = data;
      }
    )
    
    this.korisnikService.isEmployer.subscribe(
      (data:any) => {
        this.isEmployer = data;
      }
    )



  }
  
  odjava(){
    localStorage.removeItem('username');
    window.location.reload();
  }

  getBackgroundImg() {
    // replace the path to your image here
    const img = 'http://localhost:8080/profilePictures/' + this.korisnik.putDoProfilne;
    const style = `background-image: url(${img})`;
    // this is to bypass this warning 'WARNING: sanitizing unsafe style value background-image: url(assets/img/wall.png) (see http://g.co/ng/security#xss)'
    return this._sanitizer.bypassSecurityTrustStyle(style);
}

}

package hr.fer.zavrsni.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zavrsni.domain.Znanja;
import hr.fer.zavrsni.service.ZnanjaService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController

@RequestMapping("/znanja")
public class ZnanjaController {
	@Autowired
	private ZnanjaService znanjaService;
	
    @GetMapping("")
    public List<Znanja> listZnanja() {
        return znanjaService.listAll();
    }
}

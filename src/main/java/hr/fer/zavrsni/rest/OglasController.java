package hr.fer.zavrsni.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo.None;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zavrsni.dao.KorisnikPrijavljujeOglasRepository;
import hr.fer.zavrsni.domain.Firma;
import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikPrijavljujeOglas;
import hr.fer.zavrsni.domain.KorisnikRadiUFirmi;
import hr.fer.zavrsni.domain.KorisnikSpremaOglas;
import hr.fer.zavrsni.domain.Oglas;
import hr.fer.zavrsni.domain.Znanja;
import hr.fer.zavrsni.service.FirmaService;
import hr.fer.zavrsni.service.KorisnikPrijavljujeOglasService;
import hr.fer.zavrsni.service.KorisnikRadiUFirmiService;
import hr.fer.zavrsni.service.KorisnikService;
import hr.fer.zavrsni.service.OglasService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

@RequestMapping("/oglas")
public class OglasController {
	
	@Autowired
    private OglasService oglasService;
    
    @Autowired
    private KorisnikPrijavljujeOglasService kposervice;

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KorisnikRadiUFirmiService krufService;

    @Autowired
    private FirmaService firmaService;

    @Autowired
    private KorisnikPrijavljujeOglasRepository kpoRepo;
	
    @GetMapping("")
    public List<Oglas> listOglasi() {
        return oglasService.listAll();
    }

    @PostMapping("/add")
    public Oglas addOglas(@RequestBody Oglas oglas){
        return oglasService.save(oglas);
    }
    
    @GetMapping("/getKorisnici/{idOglas}")
    public List<Korisnik> getKorisnici(@PathVariable Integer idOglas){
        List<KorisnikPrijavljujeOglas> oglasi = kposervice.findByIdOglas(idOglas);
        List<Korisnik> korisnici = new ArrayList<Korisnik>();
        for (KorisnikPrijavljujeOglas oglas : oglasi){
            Korisnik k = korisnikService.findByIdKorisnik(oglas.getIdKorisnik());
            korisnici.add(k);
        }
        return korisnici;
    }

    @GetMapping("/delete/{idOglas}")
    public void deleteOglas(@PathVariable Integer idOglas){
        Oglas oglas = oglasService.findByIdOglas(idOglas);
        oglasService.delete(oglas);
    }
        
    @GetMapping("/getLogoFirme/{idOglas}")
    public String getLogoFirme(@PathVariable Integer idOglas){
        Oglas o = oglasService.findByIdOglas(idOglas);
        Korisnik poslodavac = o.getPoslodavac();
        KorisnikRadiUFirmi kruf = krufService.findbyIdKorisnik(poslodavac.getIdKorisnik());
        Firma firma = firmaService.findByIdFirma(kruf.getIdFirma());
        return firma.getPutDoLogoFirma();
    }

    @GetMapping("/getFirmaOglas/{idOglas}")
    public Firma getFirmaOglas(@PathVariable Integer idOglas){
        Oglas o = oglasService.findByIdOglas(idOglas);
        Korisnik poslodavac = o.getPoslodavac();
        KorisnikRadiUFirmi kruf = krufService.findbyIdKorisnik(poslodavac.getIdKorisnik());
        return firmaService.findByIdFirma(kruf.getIdFirma());
    }

    @PostMapping("/removeFromSpremljeni")
    public String removeFromSpremljeni(@RequestBody KorisnikSpremaOglas kso){
        oglasService.removeFromSpremljeni(kso.getIdKorisnik(), kso.getIdOglas());
        return "this";
    }
    @PostMapping("/addToSpremljeni")
    public String addToSpremljeni(@RequestBody KorisnikSpremaOglas kso){
        oglasService.addToSpremljeni(kso.getIdKorisnik(), kso.getIdOglas());
        return "this";
    }


    @GetMapping("/page/{pagesize}")
    public Page<Oglas> getAllOglasPages(@RequestParam(defaultValue = "0") Integer pageno, @PathVariable Integer pagesize){
        return oglasService.findAll(PageRequest.of(pageno, pagesize));
    }

    @GetMapping("/totalCount")
    public int getOglasCount(){
        //TODO: do this dood!
        return 0;
    }

    @GetMapping("/getAllOglasiOfUserPage/{idKorisnik}")
    public List<Oglas> listAllOglasiOfKorisnik(@PathVariable Integer idKorisnik, @RequestParam(defaultValue = "1") Integer pageno, @RequestParam(defaultValue = "30") Integer pageSize){
        List<Oglas> svi_oglasi = oglasService.listAll();
        Korisnik k = korisnikService.findByIdKorisnik(idKorisnik);
        List<Oglas> korisnik_oglasi = new ArrayList<Oglas>();

        int check = 0;
        for(Oglas o : svi_oglasi){
            check = 0;
            for (Znanja z : o.getZnanja()){
                if(!k.getZnanja().contains(z)){
                    check = 1;
                }
            }
            if (check == 0){
                korisnik_oglasi.add(o);
            }
        }
        int fromIndex = (pageno-1)*pageSize;
        int toIndex = pageno *pageSize;

        if(fromIndex > korisnik_oglasi.size()){
            fromIndex = 0;
            toIndex = 0;
        }
        if(toIndex > korisnik_oglasi.size()){
            toIndex = korisnik_oglasi.size();
        }

        List<Oglas> pagedList = korisnik_oglasi.subList(fromIndex, toIndex);
        return pagedList;
    }

    @GetMapping("/getAllOglasiOfUser/{idKorisnik}")
    public List<Oglas> listAllOglasi(@PathVariable Integer idKorisnik){
        List<Oglas> svi_oglasi = oglasService.listAll();
        Korisnik k = korisnikService.findByIdKorisnik(idKorisnik);
        List<Oglas> korisnik_oglasi = new ArrayList<Oglas>();

        int check = 0;
        for(Oglas o : svi_oglasi){
            check = 0;
            for (Znanja z : o.getZnanja()){
                if(!k.getZnanja().contains(z)){
                    check = 1;
                }
            }
            if (check == 0 && o.isAktivan() == true && o.isInEditing() == false){
                korisnik_oglasi.add(o);
            }
        }
        return korisnik_oglasi;
    }

    @GetMapping("/getAllOglasi")
    public List<Oglas> getAllOglasiActive(){
        List<Oglas> svi_oglasi = oglasService.listAll();
        List<Oglas> aktivni = new ArrayList<Oglas>();
        for(Oglas o : svi_oglasi){
            if(o.isAktivan() == true && o.isInEditing() == false){
                aktivni.add(o);
            }
        }
        return aktivni;
    }

    @GetMapping("/get/{idOglas}")
    public Oglas getById(@PathVariable Integer idOglas){
        return oglasService.findByIdOglas(idOglas);
    }

    @PostMapping("/update")
    public Oglas updateOglas(@RequestBody Oglas oglas){
        return oglasService.save(oglas);
    }

    @GetMapping("/getInEditing/{idKorisnik}")
    public List<Oglas> getInEditing(@PathVariable Integer idKorisnik){
        return oglasService.getInEditing(idKorisnik);
    }

    @GetMapping("/getPoslodavac/{idOglas}")
    public Korisnik getPoslodavac(@PathVariable Integer idOglas){
        Oglas o = oglasService.findByIdOglas(idOglas);
        return o.getPoslodavac();
    }

    @PostMapping("/removeFromPrijavljeni")
    public KorisnikPrijavljujeOglas removeFromPrijavljeni(@RequestBody KorisnikPrijavljujeOglas kpo){
        KorisnikPrijavljujeOglas kpo2 = kpoRepo.findByIdOglasAndIdKorisnik(kpo.getIdOglas(), kpo.getIdKorisnik());
        kpoRepo.delete(kpo2);
        return kpo2;
    }

}

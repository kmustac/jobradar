package hr.fer.zavrsni.rest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hr.fer.zavrsni.domain.Firma;
import hr.fer.zavrsni.domain.Komentira;
import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikListaZnanja;
import hr.fer.zavrsni.domain.KorisnikPrijavljujeOglas;
import hr.fer.zavrsni.domain.KorisnikRadiUFirmi;
import hr.fer.zavrsni.domain.KorisnikUpdateStupanj;
import hr.fer.zavrsni.domain.KorisnikZnanje;
import hr.fer.zavrsni.domain.Ocjenjuje;
import hr.fer.zavrsni.domain.Oglas;
import hr.fer.zavrsni.domain.Znanja;
import hr.fer.zavrsni.service.FirmaService;
import hr.fer.zavrsni.service.KomentiraService;
import hr.fer.zavrsni.service.KorisnikPrijavljujeOglasService;
import hr.fer.zavrsni.service.KorisnikRadiUFirmiService;
import hr.fer.zavrsni.service.KorisnikService;
import hr.fer.zavrsni.service.OcjenjujeService;
import hr.fer.zavrsni.service.OglasService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

@RequestMapping("/korisnik")
public class KorisnikController {

	@Autowired
    private KorisnikService korisnikService;
	@Autowired
    private KorisnikRadiUFirmiService krufService;
    @Autowired
    private FirmaService firmaService;
    @Autowired
    private OglasService oglasService;
    @Autowired
    private KorisnikPrijavljujeOglasService kpoService;
    @Autowired
    private OcjenjujeService ocjenjujeService;
    @Autowired
    private KomentiraService komentiraService;
	
    @GetMapping("")
    public List<Korisnik> listKorisnici() {
        return korisnikService.listAll();
    }
    
    @GetMapping("/get/{email}")
    public Korisnik getKorisnikByEmail(@PathVariable String email) {
    	return korisnikService.findByEmailKorisnik(email);
    }

    @GetMapping("/getById/{idKorisnik}")
    public Korisnik getKorisnikById(@PathVariable Integer idKorisnik){
        return korisnikService.findByIdKorisnik(idKorisnik);
    }
    
    @PostMapping("/add")
    public Korisnik addKorisnik(@RequestBody Korisnik korisnik) {
    	Korisnik kor = korisnik;
    	kor.setPutDoProfilne("noImg.png");
    	return korisnikService.save(kor);
    }
    
    @PostMapping("/add/radiUFirmi")
    public KorisnikRadiUFirmi addKruf(@RequestBody KorisnikRadiUFirmi kruf) {
    	return krufService.save(kruf);
    }
    
    @PostMapping("/updateStupanj")
    public void updateStupanj(@RequestBody KorisnikUpdateStupanj kus) {
    	korisnikService.updateStupanj(kus.getIdKorisnik(), kus.getIdStupanj());
    }
    
    @PostMapping("/removeZnanje")
    public void removeZnanje(@RequestBody KorisnikZnanje kz) {
    	korisnikService.removeZnanje(kz.getIdKorisnik(), kz.getIdZnanje());
    }
    
    @PostMapping("/addZnanje")
    public void addZnanje(@RequestBody KorisnikZnanje kz) {
    	korisnikService.addZnanje(kz.getIdKorisnik(), kz.getIdZnanje());
    }
    
    @PostMapping("/modifyZnanja")
    public void modifyZnanja(@RequestBody KorisnikListaZnanja kznanja) {
    	List<Znanja> addZnanja = kznanja.getAddZnanja();
    	List<Znanja> removeZnanja = kznanja.getRemoveZnanja();
    	
    	for(Iterator<Znanja> znanjaIt = addZnanja.iterator(); znanjaIt.hasNext(); ) {
    		Znanja znanje = znanjaIt.next();
    		korisnikService.addZnanje(kznanja.getIdKorisnik(), znanje.getIdZnanje());
    	}
    	
    	for(Iterator<Znanja> znanjaIt = removeZnanja.iterator(); znanjaIt.hasNext(); ) {
    		Znanja znanje = znanjaIt.next();
    		korisnikService.removeZnanje(kznanja.getIdKorisnik(), znanje.getIdZnanje());
    	}
    }
    
    @PostMapping("/addPic")
    public String setPic(MultipartFile file) {
    	try {
    		byte[] bytes = file.getBytes();
    		Path path = Paths.get("profilePictures\\" + file.getOriginalFilename());
    		Files.write(path, bytes);
    	}catch(IOException e) {
    		e.printStackTrace();
    	}
    	
    	String s = file.getOriginalFilename().split(".png")[0];
    	korisnikService.replaceImg(file.getOriginalFilename(), s);
    	return "she";
    }

    @PostMapping("/uploadPdf")
    public String setPdf(MultipartFile file){
        try{
            byte[] bytes = file.getBytes();
            Path path = Paths.get("src/main/resources/resources/pdfCVs/" + file.getOriginalFilename());
            Files.write(path, bytes);
        }catch(IOException e){
            e.printStackTrace();
        }
        return "she";
    }
    
    @GetMapping("/isEmployer/{idKorisnik}")
    public KorisnikRadiUFirmi isEmployer(@PathVariable Integer idKorisnik) {
    	return krufService.findbyIdKorisnik(idKorisnik);
    }

    @GetMapping("/getFirma/{idKorisnik}")
    public Firma getFirma(@PathVariable Integer idKorisnik){
        KorisnikRadiUFirmi kruf = krufService.findbyIdKorisnik(idKorisnik);
        return firmaService.findByIdFirma(kruf.getIdFirma());
    }

    @GetMapping("/getOglasi/{emailKorisnik}")
    public List<Oglas> getOglasi(@PathVariable String emailKorisnik){
        Korisnik k = korisnikService.findByEmailKorisnik(emailKorisnik);
        return k.getObjavljeniOglasi();
    }

    @GetMapping("/getSpremljeniOglasi/{emailKorisnik}")
    public List<Oglas> getSpremljeniOglasi(@PathVariable String emailKorisnik){
        Korisnik k = korisnikService.findByEmailKorisnik(emailKorisnik);
        return k.getSpremljeniOglasi();
    }
    @GetMapping("/getPrijavljeniOglasi/{emailKorisnik}")
    public List<Oglas> getPrijavljeniOglasi(@PathVariable String emailKorisnik){
        Korisnik k = korisnikService.findByEmailKorisnik(emailKorisnik);
        List<Integer> idieviOglasa = korisnikService.getPrijavljeniOglasi(k.getIdKorisnik());
        List<Oglas> oglasi = new ArrayList<Oglas>();
        for(Integer idOglas : idieviOglasa){
            oglasi.add(oglasService.findByIdOglas(idOglas));
        }
        return oglasi;
    }

    @PostMapping("/prijaviOglas/")
    public KorisnikPrijavljujeOglas prijaviOglas(@RequestBody KorisnikPrijavljujeOglas kpo){
        return kpoService.add(kpo);
    }

    @PostMapping("/dodajOcjenu/")
    public Ocjenjuje dodajOcjenu(@RequestBody Ocjenjuje ocjenjuje){
        return ocjenjujeService.save(ocjenjuje);
    }

    @PostMapping("/dodajKomentar/")
    public Komentira dodajKomentar(@RequestBody Komentira komentira){
        return komentiraService.save(komentira);
    }

    @GetMapping("/ukloniKomentar/")
    public void ukloniKomentar(@RequestBody Komentira komentira){
        komentiraService.delete(komentira);
    }

    @PostMapping("/jeLiOcjenio/")
    public Ocjenjuje jeLiOcjenio(@RequestBody Ocjenjuje ocjenjuje){
        return ocjenjujeService.jeLiOcjenio(ocjenjuje.getIdKorisnik(), ocjenjuje.getIdFirma());
    }

    @PostMapping("/jeKomentirao/")
    public Komentira jeKomentirao(@RequestBody Komentira komentira){
        return komentiraService.jeKomentirao(komentira.getIdKorisnik(), komentira.getIdFirma());
    }
    
}

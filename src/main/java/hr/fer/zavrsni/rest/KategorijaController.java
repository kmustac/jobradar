package hr.fer.zavrsni.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zavrsni.domain.Kategorija;
import hr.fer.zavrsni.domain.Znanja;
import hr.fer.zavrsni.service.KategorijaService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

@RequestMapping("/kategorija")
public class KategorijaController {
	
	@Autowired
	private KategorijaService kategorijaService;
	
    @GetMapping("")
    public List<Kategorija> listKategorija() {
        return kategorijaService.listAll();
    }
    @GetMapping("/getZnanja/{idKategorija}")
    public List<Znanja> listZnanja(@PathVariable Integer idKategorija){
    	Kategorija kat = kategorijaService.findByIdKategorija(idKategorija);
    	return kat.getZnanja();
    }
}

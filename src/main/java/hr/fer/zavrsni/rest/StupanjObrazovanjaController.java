package hr.fer.zavrsni.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zavrsni.domain.StupanjObrazovanja;
import hr.fer.zavrsni.service.StupanjObrazovanjaService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

@RequestMapping("/stupanjObr")
public class StupanjObrazovanjaController {
	
	@Autowired
	private StupanjObrazovanjaService suObrService;
	
    @GetMapping("")
    public List<StupanjObrazovanja> listKorisnici() {
        return suObrService.listAll();
    }
	
}

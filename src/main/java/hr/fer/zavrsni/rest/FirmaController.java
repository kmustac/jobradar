package hr.fer.zavrsni.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zavrsni.domain.Firma;
import hr.fer.zavrsni.domain.Komentira;
import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikRadiUFirmi;
import hr.fer.zavrsni.domain.Ocjenjuje;
import hr.fer.zavrsni.domain.Oglas;
import hr.fer.zavrsni.service.FirmaService;
import hr.fer.zavrsni.service.KomentiraService;
import hr.fer.zavrsni.service.KorisnikRadiUFirmiService;
import hr.fer.zavrsni.service.KorisnikService;
import hr.fer.zavrsni.service.OcjenjujeService;
import hr.fer.zavrsni.service.OglasService;



@CrossOrigin(origins = "http://localhost:4200")
@RestController

@RequestMapping("/firma")
public class FirmaController {

	@Autowired
    private FirmaService firmaService;
    
    @Autowired
    private OglasService oglasService;

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KorisnikRadiUFirmiService krufService;

    @Autowired
    private OcjenjujeService ocjenjujeService;

    @Autowired
    private KomentiraService komentiraService;
	
    @GetMapping("")
    public List<Firma> listFirme() {
        return firmaService.listAll();
    }

    @GetMapping("/getById/{idFirma}")
    public Firma getById(@PathVariable Integer idFirma){
        return firmaService.findByIdFirma(idFirma);
    }
    
    @GetMapping("/get/{imeFirma}")
    public Firma getFirma(@PathVariable String imeFirma) {
    	return firmaService.findByImeFirma(imeFirma);
    }
    
    @GetMapping("/getContain/{imeFirma}")
    public List<Firma> getByImeContaining(@PathVariable String imeFirma){
    	return firmaService.findByImeFirmaContaining(imeFirma);
    }
    
    @PostMapping("/add")
    public Firma addFirma(@RequestBody Firma firma) {
    	return firmaService.save(firma);
    }

    @GetMapping("/getAllOglasiOfFirma/{idFirma}")
    public List<Oglas> getAllOglasiOfFirma(@PathVariable Integer idFirma){
        List<KorisnikRadiUFirmi> krufList = krufService.findByIdFirma(idFirma);
        List<Oglas> svi_oglasi = new ArrayList<Oglas>();

        for (KorisnikRadiUFirmi kruf : krufList){
            Korisnik k = korisnikService.findByIdKorisnik(kruf.getIdKorisnik());
            for (Oglas o : k.getObjavljeniOglasi()){
                if(o.isInEditing() == false)
                    svi_oglasi.add(o);
            }
        }   
        return svi_oglasi;
    }

    @GetMapping("/getAllAktualni/{idFirma}")
    public List<Oglas> getAllAktualni(@PathVariable Integer idFirma){
        List<KorisnikRadiUFirmi> krufList = krufService.findByIdFirma(idFirma);
        List<Oglas> svi_oglasi = new ArrayList<Oglas>();

        for (KorisnikRadiUFirmi kruf : krufList){
            Korisnik k = korisnikService.findByIdKorisnik(kruf.getIdKorisnik());
            for (Oglas o : k.getObjavljeniOglasi()){
                if(o.isInEditing() == false && o.isAktivan() == true)
                    svi_oglasi.add(o);
            }
        }
        return svi_oglasi;
    }

    @GetMapping("/getOcjena/{idFirma}")
    public int getOcjena(@PathVariable Integer idFirma){
        List<Ocjenjuje> sveOcjene = ocjenjujeService.findByIdFirma(idFirma);

        int suma = 0;
        for(Ocjenjuje o : sveOcjene){
            suma += o.getOcjena();
        }
        if(sveOcjene.size() > 0)
            return suma / sveOcjene.size();
        else
            return 0;
    }

    @GetMapping("/getKomentari/{idFirma}")
    public List<Komentira> getKomentari(@PathVariable Integer idFirma){
        return komentiraService.findByIdFirma(idFirma);
    }
}

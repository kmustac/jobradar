package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "kategorija")
public class Kategorija implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idKategorija")
	private Integer idKategorija;
	
	@Column(name = "imeKategorija")
	private String imeKategorija;
	
	@OneToMany(mappedBy = "kategorija")
	private List<Znanja> znanja;
	
	public List<Znanja> getZnanja() {
		return znanja;
	}

	public void setZnanja(List<Znanja> znanja) {
		this.znanja = znanja;
	}

	public Kategorija() {
		// TODO Auto-generated constructor stub
	}

	public Kategorija(String imeKategorija) {
		super();
		this.imeKategorija = imeKategorija;
	}

	public Integer getIdKategorija() {
		return idKategorija;
	}

	public void setIdKategorija(Integer idKategorija) {
		this.idKategorija = idKategorija;
	}

	public String getImeKategorija() {
		return imeKategorija;
	}

	public void setImeKategorija(String imeKategorija) {
		this.imeKategorija = imeKategorija;
	}
	
	

}

package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "korisnikRadiUFirmi")
@IdClass(KorisnikRadiUFirmiKey.class)
public class KorisnikRadiUFirmi implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idKorisnik")
	private Integer idKorisnik;
	
	@Id
	@Column(name = "idFirma")
	private Integer idFirma;
	
	@Id
	@Column(name = "datumRada")
	private Date datumRada;
	
	@ManyToOne
	@JoinColumn(name = "idKorisnik", insertable = false, updatable = false, referencedColumnName="idKorisnik")
	@JsonIgnore
	private Korisnik korisnikZaposlenik;
	
	@ManyToOne
	@JoinColumn(name = "idFirma", insertable = false, updatable = false, referencedColumnName = "idFirma")
	@JsonIgnore
	private Firma firma;
	
	public KorisnikRadiUFirmi() {
		// TODO Auto-generated constructor stub
	}

	public KorisnikRadiUFirmi(Integer idKorisnik, Integer idFirma, Date datumRada) {
		super();
		this.idKorisnik = idKorisnik;
		this.idFirma = idFirma;
		this.datumRada = datumRada;
	}

	public Integer getIdKorisnik() {
		return idKorisnik;
	}

	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}

	public Integer getIdFirma() {
		return idFirma;
	}

	public void setIdFirma(Integer idFirma) {
		this.idFirma = idFirma;
	}

	public Date getDatumRada() {
		return datumRada;
	}

	public void setDatumRada(Date datumRada) {
		this.datumRada = datumRada;
	}

	public Korisnik getKorisnikZaposlenik() {
		return korisnikZaposlenik;
	}

	public void setKorisnikZaposlenik(Korisnik korisnikZaposlenik) {
		this.korisnikZaposlenik = korisnikZaposlenik;
	}

	public Firma getFirma() {
		return firma;
	}

	public void setFirma(Firma firma) {
		this.firma = firma;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}

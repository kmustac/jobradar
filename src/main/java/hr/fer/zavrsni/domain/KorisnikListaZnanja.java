package hr.fer.zavrsni.domain;

import java.util.List;

public class KorisnikListaZnanja {
	private Integer idKorisnik;
	private List<Znanja> addZnanja;
	private List<Znanja> removeZnanja;
	
	public KorisnikListaZnanja(Integer idKorisnik, List<Znanja> addZnanja, List<Znanja> removeZnanja) {
		super();
		this.idKorisnik = idKorisnik;
		this.addZnanja = addZnanja;
		this.removeZnanja = removeZnanja;
	}
	public KorisnikListaZnanja() {
		super();
	}
	public Integer getIdKorisnik() {
		return idKorisnik;
	}
	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}
	public List<Znanja> getAddZnanja() {
		return addZnanja;
	}
	public void setAddZnanja(List<Znanja> addZnanja) {
		this.addZnanja = addZnanja;
	}
	public List<Znanja> getRemoveZnanja() {
		return removeZnanja;
	}
	public void setRemoveZnanja(List<Znanja> removeZnanja) {
		this.removeZnanja = removeZnanja;
	}
	
	
}

package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ocjenjuje")
@IdClass(OcjenjujeKey.class)
public class Ocjenjuje implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idKorisnik")
	private Integer idKorisnik;

	@Id
	@Column(name = "idFirma")
	private Integer idFirma;
	
	@Column(name = "datumOcjene")
	private Date datumOcjene;
	
	@Column(name = "ocjena")
	private Integer ocjena;
	

	@ManyToOne
	@JoinColumn(name = "idKorisnik", insertable = false, updatable = false, referencedColumnName = "idKorisnik")
	@JsonIgnore
	private Korisnik korisnik;
	
	@ManyToOne
	@JoinColumn(name = "idFirma", insertable = false, updatable = false, referencedColumnName = "idFirma")
	@JsonIgnore
	private Firma firma;
	
	
	public Ocjenjuje() {
		// TODO Auto-generated constructor stub
	}


	public Ocjenjuje(Integer idKorisnik, Integer idFirma, Date datumOcjene, Integer ocjena, Korisnik korisnik,
			Firma firma) {
		super();
		this.idKorisnik = idKorisnik;
		this.idFirma = idFirma;
		this.datumOcjene = datumOcjene;
		this.ocjena = ocjena;
		this.korisnik = korisnik;
		this.firma = firma;
	}


	public Ocjenjuje(Integer idKorisnik, Integer idFirma, Date datumOcjene, Integer ocjena) {
		super();
		this.idKorisnik = idKorisnik;
		this.idFirma = idFirma;
		this.datumOcjene = datumOcjene;
		this.ocjena = ocjena;
	}


	public Integer getIdKorisnik() {
		return idKorisnik;
	}


	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}


	public Integer getIdFirma() {
		return idFirma;
	}


	public void setIdFirma(Integer idFirma) {
		this.idFirma = idFirma;
	}


	public Date getDatumOcjene() {
		return datumOcjene;
	}


	public void setDatumOcjene(Date datumOcjene) {
		this.datumOcjene = datumOcjene;
	}


	public Integer getOcjena() {
		return ocjena;
	}


	public void setOcjena(Integer ocjena) {
		this.ocjena = ocjena;
	}


	public Korisnik getKorisnik() {
		return korisnik;
	}


	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}


	public Firma getFirma() {
		return firma;
	}


	public void setPoslodavac(Firma firma) {
		this.firma = firma;
	}
	
	

}

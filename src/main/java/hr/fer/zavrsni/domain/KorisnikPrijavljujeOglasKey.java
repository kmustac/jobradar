package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;

public class KorisnikPrijavljujeOglasKey implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer idKorisnik;
	private Integer idOglas;
	private Date datumPrijave;
}

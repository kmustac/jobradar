package hr.fer.zavrsni.domain;

public class KorisnikZnanje {
	
	private Integer idKorisnik;
	private Integer idZnanje;
	
	public Integer getIdKorisnik() {
		return idKorisnik;
	}
	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}
	public Integer getIdZnanje() {
		return idZnanje;
	}
	public void setIdZnanje(Integer idZnanje) {
		this.idZnanje = idZnanje;
	}
	public KorisnikZnanje(Integer idKorisnik, Integer idZnanje) {
		super();
		this.idKorisnik = idKorisnik;
		this.idZnanje = idZnanje;
	}
	public KorisnikZnanje() {
		super();
	}
	
	
}

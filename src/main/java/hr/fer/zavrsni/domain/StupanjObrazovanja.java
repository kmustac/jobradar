package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("serial")
@Entity
@Table(name = "stupanjobrazovanja")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class StupanjObrazovanja implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idStupanj;
	
	@Column(name = "imeStupanj")
	private String imeStupanj;
	
	@JsonIgnore
	@OneToMany(mappedBy = "stupanjObrazovanja", fetch = FetchType.LAZY)
	private List<Korisnik> korisnici;
	
	public StupanjObrazovanja() {
		super();
	}

	public StupanjObrazovanja(String imeStupanj) {
		super();
		this.imeStupanj = imeStupanj;
	}

	public Integer getIdStupanj() {
		return idStupanj;
	}

	public void setIdStupanj(Integer idStupanj) {
		this.idStupanj = idStupanj;
	}

	public String getImeStupanj() {
		return imeStupanj;
	}

	public void setImeStupanj(String imeStupanj) {
		this.imeStupanj = imeStupanj;
	}

	public List<Korisnik> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(List<Korisnik> korisnici) {
		this.korisnici = korisnici;
	}
	
	
	
	

}

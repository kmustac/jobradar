package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "korisnikPrijavljujeOglas")
@IdClass(KorisnikPrijavljujeOglasKey.class)
public class KorisnikPrijavljujeOglas implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idKorisnik")
	private Integer idKorisnik;
	
	@Id
	@Column(name = "idOglas")
	private Integer idOglas;
	
	@Column(name = "datumPrijave")
	private Date datumPrijave;
	
	@ManyToOne
	@JoinColumn(name = "idKorisnik", insertable = false, updatable = false, referencedColumnName = "idKorisnik")
	@JsonIgnore
	private Korisnik korisnik;
	
	@ManyToOne
	@JoinColumn(name = "idOglas", insertable = false, updatable = false, referencedColumnName = "idOglas")
	@JsonIgnore
	private Oglas oglas;
	
	public KorisnikPrijavljujeOglas() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdKorisnik() {
		return idKorisnik;
	}

	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}

	public Integer getIdOglas() {
		return idOglas;
	}

	public void setIdOglas(Integer idOglas) {
		this.idOglas = idOglas;
	}

	public Date getDatumPrijave() {
		return datumPrijave;
	}

	public void setDatumPrijave(Date datumPrijave) {
		this.datumPrijave = datumPrijave;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Oglas getOglas() {
		return oglas;
	}

	public void setOglas(Oglas oglas) {
		this.oglas = oglas;
	}

	public KorisnikPrijavljujeOglas(Integer idKorisnik, Integer idOglas, Date datumPrijave) {
		super();
		this.idKorisnik = idKorisnik;
		this.idOglas = idOglas;
		this.datumPrijave = datumPrijave;
	}
	
	

}

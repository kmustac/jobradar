package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "komentira")
@IdClass(KomentiraKey.class)
public class Komentira implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idKorisnik")
	private Integer idKorisnik;

	@Id
	@Column(name = "idFirma")
	private Integer idFirma;
	
	@Column(name = "datumKomentiranja")
	private Date datumKomentiranja;
	
	@Column(name = "komentar")
	private String komentar;
	

	@ManyToOne
	@JoinColumn(name = "idKorisnik", insertable = false, updatable = false, referencedColumnName = "idKorisnik")
	@JsonIgnore
	private Korisnik korisnik;
	
	@ManyToOne
	@JoinColumn(name = "idFirma", insertable = false, updatable = false, referencedColumnName = "idFirma")
	@JsonIgnore
	private Firma firma;
	
	public Komentira() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdKorisnik() {
		return idKorisnik;
	}

	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}

	public Integer getIdFirma() {
		return idFirma;
	}

	public void setIdFirma(Integer idFirma) {
		this.idFirma = idFirma;
	}

	public Date getDatumKomentiranja() {
		return datumKomentiranja;
	}

	public void setDatumKomentiranja(Date datumKomentiranja) {
		this.datumKomentiranja = datumKomentiranja;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public Komentira(Integer idKorisnik, Integer idFirma, Date datumKomentiranja, String komentar) {
		super();
		this.idKorisnik = idKorisnik;
		this.idFirma = idFirma;
		this.datumKomentiranja = datumKomentiranja;
		this.komentar = komentar;
	}

	public Komentira(String komentar) {
		super();
		this.komentar = komentar;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Firma getPoslodavac() {
		return this.firma;
	}

	public void setPoslodavac(Firma firma) {
		this.firma = firma;
	}
	
	
	
}

package hr.fer.zavrsni.domain;

public class KorisnikUpdateStupanj {
	
	private Integer idKorisnik;
	private Integer idStupanj;
	
	public Integer getIdKorisnik() {
		return idKorisnik;
	}
	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}
	public Integer getIdStupanj() {
		return idStupanj;
	}
	public void setIdStupanj(Integer idStupanj) {
		this.idStupanj = idStupanj;
	}
	
	public KorisnikUpdateStupanj(Integer idKorisnik, Integer idStupanj) {
		super();
		this.idKorisnik = idKorisnik;
		this.idStupanj = idStupanj;
	}
	public KorisnikUpdateStupanj() {
		super();
	}
	
	
	
}

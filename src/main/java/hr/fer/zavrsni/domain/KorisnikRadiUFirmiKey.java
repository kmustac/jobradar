package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;


public class KorisnikRadiUFirmiKey implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer idKorisnik;
	private Integer idFirma;
	private Date datumRada;
}

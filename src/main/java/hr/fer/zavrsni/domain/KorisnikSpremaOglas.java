package hr.fer.zavrsni.domain;

public class KorisnikSpremaOglas{
    private Integer idKorisnik;
    private Integer idOglas;

    public Integer getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(Integer idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public Integer getIdOglas() {
        return idOglas;
    }

    public void setIdOglas(Integer idOglas) {
        this.idOglas = idOglas;
    }

    public KorisnikSpremaOglas(Integer idKorisnik, Integer idOglas) {
		super();
		this.idKorisnik = idKorisnik;
		this.idOglas= idOglas;
	}
	public KorisnikSpremaOglas(){
		super();
	}
	
    
}
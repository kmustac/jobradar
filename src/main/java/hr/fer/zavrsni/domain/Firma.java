package hr.fer.zavrsni.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "firma")
public class Firma implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idFirma;
	
	@Column(name = "imeFirma")
	private String imeFirma;
	
	@Column(name = "emailFirma")
	private String emailFirma;
	
	@Column(name = "telefonFirma")
	private String telefonFirma;
	
	@Column(name = "putDoLogoFirma")
	private String putDoLogoFirma;

	public Firma() {
		super();
	}

	public Firma(String imeFirma, String emailFirma, String telefonFirma, String putDoLogoFirma) {
		super();
		this.imeFirma = imeFirma;
		this.emailFirma = emailFirma;
		this.telefonFirma = telefonFirma;
		this.putDoLogoFirma = putDoLogoFirma;
	}

	public Integer getIdFirma() {
		return idFirma;
	}

	public void setIdFirma(Integer idFirma) {
		this.idFirma = idFirma;
	}

	public String getImeFirma() {
		return imeFirma;
	}

	public void setImeFirma(String imeFirma) {
		this.imeFirma = imeFirma;
	}

	public String getEmailFirma() {
		return emailFirma;
	}

	public void setEmailFirma(String emailFirma) {
		this.emailFirma = emailFirma;
	}

	public String getTelefonFirma() {
		return telefonFirma;
	}

	public void setTelefonFirma(String telefonFirma) {
		this.telefonFirma = telefonFirma;
	}

	public String getPutDoLogoFirma() {
		return putDoLogoFirma;
	}

	public void setPutDoLogoFirma(String putDoLogoFirma) {
		this.putDoLogoFirma = putDoLogoFirma;
	}
	
	
}

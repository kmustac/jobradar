package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@SuppressWarnings("serial")
@Entity
@Table(name = "korisnik")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Korisnik implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idKorisnik;
	
	@Column(name = "imeKorisnik")
	private String imeKorisnik;
	
	@Column(name = "prezimeKorisnik")
	private String prezimeKorisnik;
	
	@Column(name = "emailKorisnik")
	private String emailKorisnik;
	
	@Column(name = "lozinkaKorisnik")
	private String lozinkaKorisnik;
	
	@Column(name = "putDoProfilne")
	private String putDoProfilne;
	
	@Column(name = "putDoCV")
	private String putDoCV;
	
	@Column(name = "jeAdmin")
	private boolean jeAdmin;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idStupanj", nullable = false)
	private StupanjObrazovanja stupanjObrazovanja;
	
	@OneToMany(mappedBy = "poslodavac")
	@JsonManagedReference
	private List<Oglas> objavljeniOglasi;
	
	@ManyToMany
	@JoinTable(name = "korisnikZnanja",
	joinColumns = @JoinColumn(name = "idKorisnik"), 
	inverseJoinColumns = @JoinColumn(name = "idZnanje"))
	private List<Znanja> znanja;

	@ManyToMany
	@JoinTable(name = "korisnikSpremaOglas",
	joinColumns = @JoinColumn(name = "idKorisnik"), 
	inverseJoinColumns = @JoinColumn(name = "idOglas"))
	@JsonIgnore
	private List<Oglas> spremljeniOglasi;
	
	public Korisnik() {
		super();
	}

	public Korisnik(String imeKorisnik, String prezimeKorisnik, String emailKorisnik, String lozinkaKorisnik,
			String putDoProfilne, String putDoCV) {
		super();
		this.imeKorisnik = imeKorisnik;
		this.prezimeKorisnik = prezimeKorisnik;
		this.emailKorisnik = emailKorisnik;
		this.lozinkaKorisnik = lozinkaKorisnik;
		this.putDoProfilne = putDoProfilne;
		this.putDoCV = putDoCV;
	}

	public String getImeKorisnik() {
		return imeKorisnik;
	}

	public void setImeKorisnik(String imeKorisnik) {
		this.imeKorisnik = imeKorisnik;
	}

	public String getPrezimeKorisnik() {
		return prezimeKorisnik;
	}

	public void setPrezimeKorisnik(String prezimeKorisnik) {
		this.prezimeKorisnik = prezimeKorisnik;
	}

	public String getEmailKorisnik() {
		return emailKorisnik;
	}

	public void setEmailKorisnik(String emailKorisnik) {
		this.emailKorisnik = emailKorisnik;
	}

	public String getLozinkaKorisnik() {
		return lozinkaKorisnik;
	}

	public void setLozinkaKorisnik(String lozinkaKorisnik) {
		this.lozinkaKorisnik = lozinkaKorisnik;
	}

	public String getPutDoProfilne() {
		return putDoProfilne;
	}

	public void setPutDoProfilne(String putDoProfilne) {
		this.putDoProfilne = putDoProfilne;
	}

	public String getPutDoCV() {
		return putDoCV;
	}

	public void setPutDoCV(String putDoCV) {
		this.putDoCV = putDoCV;
	}

	public StupanjObrazovanja getStupanjObrazovanja() {
		return stupanjObrazovanja;
	}

	public void setStupanjObrazovanja(StupanjObrazovanja stupanjObrazovanja) {
		this.stupanjObrazovanja = stupanjObrazovanja;
	}

	public Integer getIdKorisnik() {
		return idKorisnik;
	}

	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}

	public boolean isJeAdmin() {
		return jeAdmin;
	}

	public void setJeAdmin(boolean jeAdmin) {
		this.jeAdmin = jeAdmin;
	}

	public List<Oglas> getObjavljeniOglasi() {
		return objavljeniOglasi;
	}

	public void setObjavljeniOglasi(List<Oglas> objavljeniOglasi) {
		this.objavljeniOglasi = objavljeniOglasi;
	}

	public List<Znanja> getZnanja() {
		return znanja;
	}

	public void setZnanja(List<Znanja> znanja) {
		this.znanja = znanja;
	}

	public List<Oglas> getSpremljeniOglasi() {
		return spremljeniOglasi;
	}

	public void setSpremljeniOglasi(List<Oglas> spremljeniOglasi) {
		this.spremljeniOglasi = spremljeniOglasi;
	}
	
	
	
	
}

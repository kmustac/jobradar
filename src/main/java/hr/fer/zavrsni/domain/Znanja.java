package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "znanja")
public class Znanja implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idZnanje")
	private Integer idZnanje;
	
	@Column(name = "imeZnanje")
	private String imeZnanje;
	
	//TODO: idKategorija thingy
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idKategorija", nullable = false)
	@JsonIgnore
	private Kategorija kategorija;
	
	@ManyToMany
	@JoinTable(name = "korisnikZnanja",
	joinColumns = @JoinColumn(name = "idZnanje"), 
	inverseJoinColumns = @JoinColumn(name = "idKorisnik"))
	@JsonIgnore
	private List<Korisnik> korisnici;
	
	@ManyToMany
	@JoinTable(name = "oglasTraziZnanja",
	joinColumns = @JoinColumn(name = "idZnanje"), 
	inverseJoinColumns = @JoinColumn(name = "idOglas"))
	@JsonIgnore
	private List<Oglas> oglasi;
	
	
	
	public Znanja() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdZnanje() {
		return idZnanje;
	}

	public void setIdZnanje(Integer idZnanje) {
		this.idZnanje = idZnanje;
	}

	public String getImeZnanje() {
		return imeZnanje;
	}

	public void setImeZnanje(String imeZnanje) {
		this.imeZnanje = imeZnanje;
	}

	public Znanja(String imeZnanje) {
		super();
		this.imeZnanje = imeZnanje;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	public List<Korisnik> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(List<Korisnik> korisnici) {
		this.korisnici = korisnici;
	}

	public List<Oglas> getOglasi() {
		return oglasi;
	}

	public void setOglasi(List<Oglas> oglasi) {
		this.oglasi = oglasi;
	}
	
	

}

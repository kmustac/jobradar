package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@SuppressWarnings("serial")
@Entity
@Table(name = "oglas")
public class Oglas implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idOglas;
	
	@Column(name = "datumObjave")
	private Date datumObjave;
	
	@Column(name = "nazivOglasa")
	private String nazivOglasa;
	
	@Column(name = "tekstOglasa")
	private String tekstOglasa;
	
	@Column(name = "datumRoka")
	private Date datumRoka;
	
	@Column(name = "aktivan")
	private boolean aktivan;

	@Column(name = "inEditing")
	private boolean inEditing;
	
	//TODO: idPoslodavac
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPoslodavac", nullable = false)
	@JsonBackReference
	private Korisnik poslodavac;
	
	@ManyToMany
	@JoinTable(name = "oglasTraziZnanja",
	joinColumns = @JoinColumn(name = "idOglas"), 
	inverseJoinColumns = @JoinColumn(name = "idZnanje"))
	private List<Znanja> znanja;
	
	@ManyToMany
	@JoinTable(name = "korisnikSpremaOglas",
	joinColumns = @JoinColumn(name = "idOglas"), 
	inverseJoinColumns = @JoinColumn(name = "idKorisnik"))
	@JsonIgnore
	private List<Korisnik> korisnici;
	
	public Oglas() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Oglas(Date datumObjave, String nazivOglasa, String tekstOglasa, Date datumRoka, boolean aktivan, boolean inEditing) {
		super();
		this.datumObjave = datumObjave;
		this.nazivOglasa = nazivOglasa;
		this.tekstOglasa = tekstOglasa;
		this.datumRoka = datumRoka;
		this.aktivan = aktivan;
		this.inEditing = inEditing;
	}

	public Integer getIdOglas() {
		return idOglas;
	}

	public void setIdOglas(Integer idOglas) {
		this.idOglas = idOglas;
	}

	public Date getDatumObjave() {
		return datumObjave;
	}

	public void setDatumObjave(Date datumObjave) {
		this.datumObjave = datumObjave;
	}

	public String getNazivOglasa() {
		return nazivOglasa;
	}

	public void setNazivOglasa(String nazivOglasa) {
		this.nazivOglasa = nazivOglasa;
	}

	public String getTekstOglasa() {
		return tekstOglasa;
	}

	public void setTekstOglasa(String tekstOglasa) {
		this.tekstOglasa = tekstOglasa;
	}

	public Date getDatumRoka() {
		return datumRoka;
	}

	public void setDatumRoka(Date datumRoka) {
		this.datumRoka = datumRoka;
	}

	public boolean isAktivan() {
		return aktivan;
	}

	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

	public Korisnik getPoslodavac() {
		return poslodavac;
	}

	public void setPoslodavac(Korisnik poslodavac) {
		this.poslodavac = poslodavac;
	}

	public List<Znanja> getZnanja() {
		return znanja;
	}

	public void setZnanja(List<Znanja> znanja) {
		this.znanja = znanja;
	}

	public boolean isInEditing() {
		return inEditing;
	}

	public void setInEditing(boolean inEditing) {
		this.inEditing = inEditing;
	}
	
	
}

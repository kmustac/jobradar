package hr.fer.zavrsni.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "poruka")
@IdClass(PorukaKey.class)
public class Poruka implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idKorisnik")
	private Integer idKorisnik;

	@Id
	@Column(name = "idPoslodavac")
	private Integer idPoslodavac;
	
	@Id
	@Column(name = "vrijemeSlanja")
	private Date vrijemeSlanja;
	
	@Column(name = "tekstPoruke")
	private String tekstPoruke;
	
	@ManyToOne
	@JoinColumn(name = "idKorisnik", insertable = false, updatable = false, referencedColumnName = "idKorisnik")
	@JsonIgnore
	private Korisnik korisnik;
	
	@ManyToOne
	@JoinColumn(name = "idPoslodavac", insertable = false, updatable = false, referencedColumnName = "idKorisnik")
	@JsonIgnore
	private Korisnik poslodavac;

	public Poruka() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdKorisnik() {
		return idKorisnik;
	}

	public void setIdKorisnik(Integer idKorisnik) {
		this.idKorisnik = idKorisnik;
	}

	public Integer getIdPoslodavac() {
		return idPoslodavac;
	}

	public void setIdPoslodavac(Integer idPoslodavac) {
		this.idPoslodavac = idPoslodavac;
	}

	public Date getVrijemeSlanja() {
		return vrijemeSlanja;
	}

	public void setVrijemeSlanja(Date vrijemeSlanja) {
		this.vrijemeSlanja = vrijemeSlanja;
	}

	public String getTekstPoruke() {
		return tekstPoruke;
	}

	public void setTekstPoruke(String tekstPoruke) {
		this.tekstPoruke = tekstPoruke;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Korisnik getPoslodavac() {
		return poslodavac;
	}

	public void setPoslodavac(Korisnik poslodavac) {
		this.poslodavac = poslodavac;
	}

	public Poruka(Integer idKorisnik, Integer idPoslodavac, Date vrijemeSlanja, String tekstPoruke) {
		super();
		this.idKorisnik = idKorisnik;
		this.idPoslodavac = idPoslodavac;
		this.vrijemeSlanja = vrijemeSlanja;
		this.tekstPoruke = tekstPoruke;
	}

	public Poruka(Integer idKorisnik, Integer idPoslodavac, Date vrijemeSlanja, String tekstPoruke, Korisnik korisnik,
			Korisnik poslodavac) {
		super();
		this.idKorisnik = idKorisnik;
		this.idPoslodavac = idPoslodavac;
		this.vrijemeSlanja = vrijemeSlanja;
		this.tekstPoruke = tekstPoruke;
		this.korisnik = korisnik;
		this.poslodavac = poslodavac;
	}
	
	

}

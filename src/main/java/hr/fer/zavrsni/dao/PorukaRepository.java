package hr.fer.zavrsni.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Poruka;

@Repository
public interface PorukaRepository extends JpaRepository<Poruka, Integer>{

}

package hr.fer.zavrsni.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Firma;

@Repository
public interface FirmaRepository extends JpaRepository<Firma, Integer> {
	
	Firma findByImeFirma(String imeFirma);
	List<Firma> findByImeFirmaContaining(String imeFirma);
	Firma findByIdFirma(Integer idFirma);
	
}

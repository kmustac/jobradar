package hr.fer.zavrsni.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Kategorija;

@Repository
public interface KategorijaRepository extends JpaRepository<Kategorija, Integer>{

	Kategorija findByImeKategorija(String imeKategorija);
	Kategorija findByIdKategorija(Integer idKategorija);
}

package hr.fer.zavrsni.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Oglas;

@Repository
public interface OglasRepository extends JpaRepository<Oglas, Integer>{

	List<Oglas> findByNazivOglasaContaining(String naziv);

	Oglas findByIdOglas(Integer idOglas);


	@Transactional
    @Modifying
    @Query(value = "DELETE from korisnikSpremaOglas where idKorisnik = (:idKorisnik) and idOglas = (:idOglas)", nativeQuery = true)
	void removeFromSpremljeni(Integer idKorisnik, Integer idOglas);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO korisnikSpremaOglas values ((:idKorisnik), (:idOglas))", nativeQuery = true)
	void addToSpremljeni(Integer idKorisnik, Integer idOglas);

	@Query(value = "SELECT * FROM oglas WHERE idPoslodavac = (:idKorisnik) AND inEditing = 1 AND aktivan = 0", nativeQuery = true)
	List<Oglas> getInEditing(Integer idKorisnik);
}

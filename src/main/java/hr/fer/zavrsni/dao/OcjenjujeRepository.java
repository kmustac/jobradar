package hr.fer.zavrsni.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Ocjenjuje;

@Repository
public interface OcjenjujeRepository extends JpaRepository<Ocjenjuje , Integer>{
    
    @Query(value = "select * from ocjenjuje where idFirma = (:idFirma)", nativeQuery = true)
	List<Ocjenjuje> findByIdFirma(Integer idFirma);

    @Query(value = "select * from ocjenjuje where idKorisnik = (:idKorisnik) and idFirma = (:idFirma)", nativeQuery = true)
	Ocjenjuje jeLiOcjenio(Integer idKorisnik, Integer idFirma);

}

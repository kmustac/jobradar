package hr.fer.zavrsni.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Komentira;

@Repository
public interface KomentiraRepository extends JpaRepository<Komentira, Integer>{

    @Query(value = "select * from komentira where idFirma = (:idFirma)", nativeQuery = true)
	List<Komentira> findByIdFirma(Integer idFirma);

    @Query(value = "select * from komentira where idKorisnik = (:idKorisnik) and idFirma = (:idFirma)", nativeQuery = true)
	Komentira jeKomentirao(Integer idKorisnik, Integer idFirma);

	
}

package hr.fer.zavrsni.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikPrijavljujeOglas;

@Repository
public interface KorisnikPrijavljujeOglasRepository extends JpaRepository<KorisnikPrijavljujeOglas, Integer>{

	List<KorisnikPrijavljujeOglas> findByIdOglas(Integer idOglas);

	KorisnikPrijavljujeOglas findByIdOglasAndIdKorisnik(Integer idOglas, Integer idKorisnik);

}

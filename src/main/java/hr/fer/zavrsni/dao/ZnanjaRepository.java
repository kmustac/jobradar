package hr.fer.zavrsni.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Znanja;

@Repository
public interface ZnanjaRepository extends JpaRepository<Znanja, Integer>{
	
	Znanja findByImeZnanje(String imeZnanje);
}

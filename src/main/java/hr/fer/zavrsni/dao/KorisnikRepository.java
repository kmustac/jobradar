package hr.fer.zavrsni.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikUpdateStupanj;
import hr.fer.zavrsni.domain.Oglas;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Integer> {

	Korisnik findByEmailKorisnik(String emailKorisnik);
	
	@Transactional
    @Modifying
    @Query(value = "UPDATE korisnik k set k.idStupanj = (:idStupanj) where k.idKorisnik = (:idKorisnik)", nativeQuery = true)
	void updateStupanj(Integer idKorisnik, Integer idStupanj);
	
	@Transactional
    @Modifying
    @Query(value = "DELETE from korisnikZnanja where idKorisnik = (:idKorisnik) and idZnanje = (:idZnanje)", nativeQuery = true)
	void removeZnanje(Integer idKorisnik, Integer idZnanje);
	
	@Transactional
    @Modifying
    @Query(value = "INSERT INTO korisnikZnanja VALUES((:idKorisnik), (:idZnanje))", nativeQuery = true)
	void addZnanje(Integer idKorisnik, Integer idZnanje);
	
	@Transactional
    @Modifying
    @Query(value = "UPDATE korisnik k set k.putdoprofilne = (:path) where k.emailKorisnik = (:kor)", nativeQuery = true)
    void replaceImg(String path, String kor);

	Korisnik findByIdKorisnik(Integer idKorisnik);

    @Query(value = "select idOglas from korisnikprijavljujeoglas where idKorisnik = (:idKorisnik)", nativeQuery = true)
	List<Integer> getPrijavljeniOglasi(Integer idKorisnik);
	
}

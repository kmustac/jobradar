package hr.fer.zavrsni.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikRadiUFirmi;

@Repository
public interface KorisnikRadiUFirmiRepository extends JpaRepository<KorisnikRadiUFirmi, Integer>{

	@Query(value = "select * from korisnikradiufirmi where idKorisnik = (:idKorisnik)", nativeQuery = true)
	KorisnikRadiUFirmi findByIdKorisnik(Integer idKorisnik);

	@Query(value = "select * from korisnikradiufirmi where idFirma = (:idFirma)", nativeQuery = true)
	List<KorisnikRadiUFirmi> findByIdFirma(Integer idFirma);

}

package hr.fer.zavrsni.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.domain.StupanjObrazovanja;

@Repository
public interface StupanjObrazovanjaRepository extends JpaRepository<StupanjObrazovanja, Integer> {
	
	StupanjObrazovanja findByImeStupanj(String imeStupanj);
	StupanjObrazovanja findByImeStupanjContaining(String imeStupanj);
}

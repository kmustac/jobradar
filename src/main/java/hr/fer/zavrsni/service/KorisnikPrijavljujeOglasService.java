package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.KorisnikPrijavljujeOglas;

@Service
public interface KorisnikPrijavljujeOglasService {

    List<KorisnikPrijavljujeOglas> listAll();
    KorisnikPrijavljujeOglas save(KorisnikPrijavljujeOglas KorisnikPrijavljujeOglas);
    void delete(KorisnikPrijavljujeOglas KorisnikPrijavljujeOglas);
	List<KorisnikPrijavljujeOglas> findByIdOglas(Integer idOglas);
	KorisnikPrijavljujeOglas add(KorisnikPrijavljujeOglas kpo);
}

package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Poruka;

@Service
public interface PorukaService {

    List<Poruka> listAll();
    Poruka save(Poruka Poruka);
    void delete(Poruka Poruka);
}

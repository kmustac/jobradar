package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Znanja;


@Service
public interface ZnanjaService {

    List<Znanja> listAll();
    Znanja save(Znanja znanje);
    void delete(Znanja znanje);
    Znanja findByImeZnanje(String imeZnanje);
}

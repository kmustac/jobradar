package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Komentira;


@Service
public interface KomentiraService {

    List<Komentira> listAll();
    Komentira save(Komentira komentira);
    void delete(Komentira komentira);
	List<Komentira> findByIdFirma(Integer idFirma);
	Komentira jeKomentirao(Integer idKorisnik, Integer idFirma);
	
}

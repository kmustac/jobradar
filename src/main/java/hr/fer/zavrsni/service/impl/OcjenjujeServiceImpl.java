package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.OcjenjujeRepository;
import hr.fer.zavrsni.domain.Ocjenjuje;
import hr.fer.zavrsni.service.OcjenjujeService;

@Service
public class OcjenjujeServiceImpl implements OcjenjujeService {
	
	@Autowired
	private OcjenjujeRepository ocjenjujeRepo;
	@Override
	public List<Ocjenjuje> listAll() {
		return ocjenjujeRepo.findAll();
	}

	@Override
	public Ocjenjuje save(Ocjenjuje Ocjenjuje) {
		return ocjenjujeRepo.save(Ocjenjuje);
	}

	@Override
	public void delete(Ocjenjuje Ocjenjuje) {
		ocjenjujeRepo.delete(Ocjenjuje);
	}

	@Override
	public List<Ocjenjuje> findByIdFirma(Integer idFirma) {
		return ocjenjujeRepo.findByIdFirma(idFirma);
	}

	@Override
	public Ocjenjuje jeLiOcjenio(Integer idKorisnik, Integer idFirma) {
		return ocjenjujeRepo.jeLiOcjenio(idKorisnik, idFirma);
	}

}

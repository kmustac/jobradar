package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.KorisnikRepository;
import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikUpdateStupanj;
import hr.fer.zavrsni.domain.Oglas;
import hr.fer.zavrsni.service.KorisnikService;

@Service
public class KorisnikServiceJpa implements KorisnikService {

	@Autowired
	private KorisnikRepository korRepo;

	@Override
	public List<Korisnik> listAll() {
		return korRepo.findAll();
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		return korRepo.save(korisnik);
	}

	@Override
	public void delete(Korisnik kor) {
		korRepo.delete(kor);
	}

	@Override
	public Korisnik findByEmailKorisnik(String emailKorisnik) {
		return korRepo.findByEmailKorisnik(emailKorisnik);
	}

	@Override
	public void updateStupanj(Integer idKorisnik, Integer idStupanj) {
		korRepo.updateStupanj(idKorisnik, idStupanj);
	}

	@Override
	public void removeZnanje(Integer idKorisnik, Integer idZnanje) {
		korRepo.removeZnanje(idKorisnik, idZnanje);
	}

	@Override
	public void addZnanje(Integer idKorisnik, Integer idZnanje) {
		korRepo.addZnanje(idKorisnik, idZnanje);
	}

	@Override
	public void replaceImg(String path, String kor) {
		korRepo.replaceImg(path, kor);
	}

	@Override
	public Korisnik findByIdKorisnik(Integer idKorisnik) {
		return korRepo.findByIdKorisnik(idKorisnik);
	}

	@Override
	public List<Integer> getPrijavljeniOglasi(Integer idKorisnik) {
		return korRepo.getPrijavljeniOglasi(idKorisnik);
	}

}

package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.KategorijaRepository;
import hr.fer.zavrsni.domain.Kategorija;
import hr.fer.zavrsni.service.KategorijaService;

@Service
public class KategorijaServiceImpl implements KategorijaService{

	@Autowired
	private KategorijaRepository kategorijaRepo;
	
	@Override
	public List<Kategorija> listAll() {
		return kategorijaRepo.findAll();
	}

	@Override
	public Kategorija save(Kategorija kategorija) {
		return kategorijaRepo.save(kategorija);
	}

	@Override
	public void delete(Kategorija kategorija) {
		kategorijaRepo.delete(kategorija);
	}

	@Override
	public Kategorija findByImeKategorija(String imeKategorija) {
		return kategorijaRepo.findByImeKategorija(imeKategorija);
	}

	@Override
	public Kategorija findByIdKategorija(Integer idKategorija) {
		return kategorijaRepo.findByIdKategorija(idKategorija);
	}

}

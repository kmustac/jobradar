package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.KorisnikPrijavljujeOglasRepository;
import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikPrijavljujeOglas;
import hr.fer.zavrsni.service.KorisnikPrijavljujeOglasService;

@Service
public class KorisnikPrijavljujeOglasServiceImpl implements KorisnikPrijavljujeOglasService {

	@Autowired
	private KorisnikPrijavljujeOglasRepository kpoRepo;

	@Override
	public List<KorisnikPrijavljujeOglas> listAll() {
		return kpoRepo.findAll();
	}

	@Override
	public KorisnikPrijavljujeOglas save(KorisnikPrijavljujeOglas KorisnikPrijavljujeOglas) {
		return kpoRepo.save(KorisnikPrijavljujeOglas);
	}

	@Override
	public void delete(KorisnikPrijavljujeOglas KorisnikPrijavljujeOglas) {
		kpoRepo.delete(KorisnikPrijavljujeOglas);
	}

	@Override
	public List<KorisnikPrijavljujeOglas> findByIdOglas(Integer idOglas) {
		return kpoRepo.findByIdOglas(idOglas);
	}

	@Override
	public KorisnikPrijavljujeOglas add(KorisnikPrijavljujeOglas kpo) {
		return kpoRepo.save(kpo);
	}

}

package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.ZnanjaRepository;
import hr.fer.zavrsni.domain.Znanja;
import hr.fer.zavrsni.service.ZnanjaService;

@Service
public class ZnanjaServiceImpl implements ZnanjaService{
	
	@Autowired
	private ZnanjaRepository znanjeRepo;

	@Override
	public List<Znanja> listAll() {
		return znanjeRepo.findAll();
	}

	@Override
	public Znanja save(Znanja znanje) {
		return znanjeRepo.save(znanje);
	}

	@Override
	public void delete(Znanja znanje) {
		znanjeRepo.delete(znanje);
	}

	@Override
	public Znanja findByImeZnanje(String imeZnanje) {
		return znanjeRepo.findByImeZnanje(imeZnanje);
	}
	
}

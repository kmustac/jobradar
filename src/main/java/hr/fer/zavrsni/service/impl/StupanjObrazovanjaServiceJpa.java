package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.StupanjObrazovanjaRepository;
import hr.fer.zavrsni.domain.StupanjObrazovanja;
import hr.fer.zavrsni.service.StupanjObrazovanjaService;

@Service
public class StupanjObrazovanjaServiceJpa implements StupanjObrazovanjaService {
	
	@Autowired
	private StupanjObrazovanjaRepository stuORepo;
	
	@Override
	public List<StupanjObrazovanja> listAll() {
		return stuORepo.findAll();
	}

	@Override
	public StupanjObrazovanja save(StupanjObrazovanja stupanj) {
		return stuORepo.save(stupanj);
	}

	@Override
	public void delete(StupanjObrazovanja stupanj) {
		stuORepo.delete(stupanj);
	}

	@Override
	public StupanjObrazovanja findByImeStupanj(String imeStupanj) {
		return stuORepo.findByImeStupanj(imeStupanj);
	}

	@Override
	public StupanjObrazovanja findByImeStupanjContaining(String imeStupanj) {
		return stuORepo.findByImeStupanjContaining(imeStupanj);
	}

}

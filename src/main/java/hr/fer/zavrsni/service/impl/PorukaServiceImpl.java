package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.PorukaRepository;
import hr.fer.zavrsni.domain.Poruka;
import hr.fer.zavrsni.service.PorukaService;

@Service
public class PorukaServiceImpl implements PorukaService {

	@Autowired
	private PorukaRepository porukaRepo;
	@Override
	public List<Poruka> listAll() {
		return porukaRepo.findAll();
	}

	@Override
	public Poruka save(Poruka Poruka) {
		return porukaRepo.save(Poruka);
	}

	@Override
	public void delete(Poruka Poruka) {
		porukaRepo.delete(Poruka);
	}

}

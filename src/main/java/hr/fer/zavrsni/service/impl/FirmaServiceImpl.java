package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.FirmaRepository;
import hr.fer.zavrsni.domain.Firma;
import hr.fer.zavrsni.service.FirmaService;

@Service
public class FirmaServiceImpl implements FirmaService {
	
	@Autowired
	private FirmaRepository firmaRepo;
	
	@Override
	public List<Firma> listAll() {
		return firmaRepo.findAll();
	}

	@Override
	public Firma save(Firma firma) {
		return firmaRepo.save(firma);
	}

	@Override
	public void delete(Firma firma) {
		firmaRepo.delete(firma);
	}

	@Override
	public Firma findByImeFirma(String imeFirma) {
		return firmaRepo.findByImeFirma(imeFirma);
	}

	@Override
	public List<Firma> findByImeFirmaContaining(String imeFirma) {
		return firmaRepo.findByImeFirmaContaining(imeFirma);
	}

	@Override
	public Firma findByIdFirma(Integer idFirma) {
		return firmaRepo.findByIdFirma(idFirma);
	}

}

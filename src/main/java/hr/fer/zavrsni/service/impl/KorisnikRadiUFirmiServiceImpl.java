package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.KorisnikRadiUFirmiRepository;
import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikRadiUFirmi;
import hr.fer.zavrsni.service.KorisnikRadiUFirmiService;

@Service
public class KorisnikRadiUFirmiServiceImpl implements KorisnikRadiUFirmiService {
	
	@Autowired
	private KorisnikRadiUFirmiRepository krufRepo;
	@Override
	public List<KorisnikRadiUFirmi> listAll() {
		return krufRepo.findAll();
	}

	@Override
	public KorisnikRadiUFirmi save(KorisnikRadiUFirmi KorisnikRadiUFirmi) {
		return krufRepo.save(KorisnikRadiUFirmi);
	}

	@Override
	public void delete(KorisnikRadiUFirmi KorisnikRadiUFirmi) {
		krufRepo.delete(KorisnikRadiUFirmi);
	}

	@Override
	public KorisnikRadiUFirmi findbyIdKorisnik(Integer idKorisnik) {
		return krufRepo.findByIdKorisnik(idKorisnik);
	}

	@Override
	public List<KorisnikRadiUFirmi> findByIdFirma(Integer idFirma) {
		return krufRepo.findByIdFirma(idFirma);
	}

}

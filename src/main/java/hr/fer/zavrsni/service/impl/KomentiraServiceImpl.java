package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.KomentiraRepository;
import hr.fer.zavrsni.domain.Komentira;
import hr.fer.zavrsni.service.KomentiraService;

@Service
public class KomentiraServiceImpl implements KomentiraService {

    @Autowired
    private KomentiraRepository komentiraRepo;

    @Override
    public List<Komentira> listAll() {
        return komentiraRepo.findAll();
    }

    @Override
    public Komentira save(Komentira komentira) {
        return komentiraRepo.save(komentira);
    }

    @Override
    public void delete(Komentira komentira) {
        komentiraRepo.delete(komentira);
    }

    @Override
    public List<Komentira> findByIdFirma(Integer idFirma) {
        return komentiraRepo.findByIdFirma(idFirma);
    }

    @Override
    public Komentira jeKomentirao(Integer idKorisnik, Integer idFirma) {
        return komentiraRepo.jeKomentirao(idKorisnik, idFirma);
    }
    
}
package hr.fer.zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.dao.OglasRepository;
import hr.fer.zavrsni.domain.Oglas;
import hr.fer.zavrsni.service.OglasService;

@Service
public class OglasServiceImpl implements OglasService {

	@Autowired
	private OglasRepository oglasRepo;

	@Override
	public List<Oglas> listAll() {
		return oglasRepo.findAll();
	}

	@Override
	public Oglas save(Oglas oglas) {
		return oglasRepo.save(oglas);
	}

	@Override
	public void delete(Oglas oglas) {
		oglasRepo.delete(oglas);
	}

	@Override
	public List<Oglas> findByNazivOglasaContaining(String naziv) {
		return oglasRepo.findByNazivOglasaContaining(naziv);
	}

	@Override
	public Oglas findByIdOglas(Integer idOglas) {
		return oglasRepo.findByIdOglas(idOglas);
	}

	@Override
	public void removeFromSpremljeni(Integer idKorisnik, Integer idOglas) {
		oglasRepo.removeFromSpremljeni(idKorisnik, idOglas);
	}

	@Override
	public Page<Oglas> findAll(PageRequest pageRequest) {
		return oglasRepo.findAll(pageRequest);
	}

	@Override
	public void addToSpremljeni(Integer idKorisnik, Integer idOglas) {
		oglasRepo.addToSpremljeni(idKorisnik, idOglas);
	}

	@Override
	public List<Oglas> getInEditing(Integer idKorisnik) {
		return oglasRepo.getInEditing(idKorisnik);
	}

}

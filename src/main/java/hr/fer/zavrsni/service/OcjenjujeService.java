package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Ocjenjuje;


@Service
public interface OcjenjujeService {

    List<Ocjenjuje> listAll();
    Ocjenjuje save(Ocjenjuje Ocjenjuje);
    void delete(Ocjenjuje Ocjenjuje);
	List<Ocjenjuje> findByIdFirma(Integer idFirma);
	Ocjenjuje jeLiOcjenio(Integer idKorisnik, Integer idFirma);
}

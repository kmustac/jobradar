package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikUpdateStupanj;
import hr.fer.zavrsni.domain.Oglas;

@Service
public interface KorisnikService {

    List<Korisnik> listAll();
    Korisnik save(Korisnik korisnik);
    void delete(Korisnik kor);
    Korisnik findByEmailKorisnik(String emailKorisnik);
    
    void updateStupanj(Integer idKorisnik, Integer idStupanj);
	
    void removeZnanje(Integer idKorisnik, Integer idZnanje);
    void addZnanje(Integer idKorisnik, Integer idZnanje);
    
    void replaceImg(String path, String kor);
    Korisnik findByIdKorisnik(Integer idKorisnik);
    
	List<Integer> getPrijavljeniOglasi(Integer idKorisnik);
}

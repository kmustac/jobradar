package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Oglas;


@Service
public interface OglasService {
	
    List<Oglas> listAll();
    Oglas save(Oglas oglas);
    void delete(Oglas oglas);
    List<Oglas> findByNazivOglasaContaining(String naziv);
	Oglas findByIdOglas(Integer idOglas);
    void removeFromSpremljeni(Integer idKorisnik, Integer idOglas);
    
    Page<Oglas> findAll(PageRequest pageRequest);
	void addToSpremljeni(Integer idKorisnik, Integer idOglas);
	List<Oglas> getInEditing(Integer idKorisnik);
	
}

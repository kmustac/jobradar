package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.StupanjObrazovanja;

@Service
public interface StupanjObrazovanjaService {

	List<StupanjObrazovanja> listAll();
	StupanjObrazovanja save(StupanjObrazovanja stupanj);
	void delete(StupanjObrazovanja stupanj);
	StupanjObrazovanja findByImeStupanj(String imeStupanj);
	StupanjObrazovanja findByImeStupanjContaining(String imeStupanj);
	
}

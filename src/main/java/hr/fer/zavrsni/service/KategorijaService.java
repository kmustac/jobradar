package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Kategorija;

@Service
public interface KategorijaService {

    List<Kategorija> listAll();
    Kategorija save(Kategorija kategorija);
    void delete(Kategorija kategorija);
    Kategorija findByImeKategorija(String imeKategorija);
    Kategorija findByIdKategorija(Integer idKategorija);
}

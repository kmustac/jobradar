package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Firma;


@Service
public interface FirmaService{

    List<Firma> listAll();
    Firma save(Firma firma);
    void delete(Firma firma);
    Firma findByImeFirma(String imeFirma);
    List<Firma> findByImeFirmaContaining(String imeFirma);
    Firma findByIdFirma(Integer idFirma);
	
}

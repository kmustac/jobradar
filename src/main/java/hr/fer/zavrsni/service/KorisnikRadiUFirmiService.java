package hr.fer.zavrsni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.zavrsni.domain.Korisnik;
import hr.fer.zavrsni.domain.KorisnikRadiUFirmi;

@Service
public interface KorisnikRadiUFirmiService {

    List<KorisnikRadiUFirmi> listAll();
    KorisnikRadiUFirmi save(KorisnikRadiUFirmi KorisnikRadiUFirmi);
    void delete(KorisnikRadiUFirmi KorisnikRadiUFirmi);
	KorisnikRadiUFirmi findbyIdKorisnik(Integer idKorisnik);
	List<KorisnikRadiUFirmi> findByIdFirma(Integer idFirma);
}

package hr.fer.zavrsni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobRadarApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobRadarApplication.class, args);
	}

}
